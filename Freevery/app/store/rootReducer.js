import {combineReducers} from 'redux'
import mainNavReducer from '../containers/MainNav/reducer';
import homeReducer from '../containers/Home/reducer';

const appReducer = combineReducers({
  mainNavReducer,
  homeReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'CLEAN_DB') {
    state = undefined
  }
  return appReducer(state, action)
};

export default rootReducer
