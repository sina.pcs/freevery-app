import * as storage from 'redux-storage'
import {
  createStore,
  applyMiddleware,
} from "redux"
import thunk from 'redux-thunk'
import rootReducer from './rootReducer';
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';
import {SHOULD_SAVE} from '../constants/ActionTypes'
//import Merger from '../utils/mergeStorage';

let middlewares = [];

// const reducer = storage.reducer(rootReducer);
// const engine = createEngine('footbin-key');
// const middleware = storage.createMiddleware(engine, [], SHOULD_SAVE);
// const createStoreWithMiddleware = applyMiddleware(thunk, ...middlewares, middleware)(createStore);
// export const store = createStoreWithMiddleware(reducer);
// export const load = storage.createLoader(engine);


export const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);