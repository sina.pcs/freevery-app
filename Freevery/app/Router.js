import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    AsyncStorage
} from 'react-native';
import Home from './components/Home';
import {Provider} from 'react-redux';
import configureStore from './redux/store/configureStore';
import {Scene, Router, NavBar} from 'react-native-router-flux';

const store = configureStore();


export default class AwesomeNativeBase extends React.Component {
    render() {
        return(
            <Provider store={store}>
                <Drawer
                    ref="navigation"
                    open={state.open}
                    onOpen={()=>Actions.refresh({key:state.key, open: true})}
                    onClose={()=>Actions.refresh({key:state.key, open: false})}
                    type="displace"
                    content={<SideMenu />}
                    tapToClose={true}
                    openDrawerOffset={0.2}
                    panCloseMask={0.2}
                    negotiatePan={true}
                    tweenHandler={(ratio) => ({
                        main: { opacity:Math.max(0.54,1-ratio) }
                    })}>
                    <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
                </Drawer>
                <Router>
                    <Scene key="root" >
                        <Scene key="home" navigationBarStyle={{display: 'none'}} component={Home}/>
                    </Scene>
                </Router>
            </Provider>
        )
    }
}
AppRegistry.registerComponent('AwesomeNativeBase', () => AwesomeNativeBase);

