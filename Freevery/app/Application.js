'use strict';
import React from 'react';
import {TouchableOpacity,View,Linking} from 'react-native';
import DeepLinking from 'react-native-deep-linking';
import Text from './components/Text';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as navActions from './redux/actions/navActions';
import Home from './components/Home';
import Login from './components/Login';
import Signup from './components/Signup';
import ForgotPassword from './components/ForgotPassword';
import ChangePassword from './components/ChangePassword';
import OrderList from './components/OrderList';
import OrderStatus from './components/OrderStatus';
import CheckoutForm from './components/CheckoutForm';
import Search from './components/Search';
import UserArea from './components/UserArea';
import ProfileSettings from './components/ProfileSettings';
import OrdersHistoryPage from './components/OrdersHistoryPage';
import {Icon,Drawer} from 'native-base';
import { TabNavigator,TabBarTop, StackNavigator, DrawerNavigator } from 'react-navigation';
import {colors,firstFont} from './constants/AppConstants'
import ControlPanel from './components/ControlPanel';
import Basket from './components/Basket';
import ModalUpdate from './components/ModalUpdate';

const GenericHeader = (title) => {
  return (
      <Text style={{fontSize:16,marginRight:20}}>{title}</Text>
  )
};

const Header = ({ navigation }) => {
    const { state, setParams } = navigation;
    return {
        title: (state.params && state.params.title)?state.params.title:'Freevery',
        headerTintColor:colors.base,
        headerRight: (
            <TouchableOpacity
                style={{flexDirection:'row',alignItems:'center',marginRight:0,paddingHorizontal:10,flex:1}}
                onPress={() => {navigation.navigate('DrawerOpen')}}
            >
                <Text style={{color:colors.gray46,marginRight:5,fontSize:12}}>دسته بندی محصولات</Text>
                <Icon name='ios-menu' style={{color: colors.gray46}} />
            </TouchableOpacity>
        ),
    }
};

const HomeNavigator = StackNavigator(
  {
      HomeScreen: {
        screen: Home
      }
  },
  {
      navigationOptions:Header
  }
);

const CheckoutNav = StackNavigator(
    {
        OrderList:{
            screen: OrderList,
            navigationOptions: {
                headerRight: GenericHeader('سبد خرید')
            }
        },
        Checkout:{
            screen: CheckoutForm,
            navigationOptions: {
                headerRight: GenericHeader('مشخصات ارسال')
            }
        },
    },
);

const ProfileNavigator = StackNavigator(
    {
        UserArea:{
            screen: UserArea,
            navigationOptions: {
                header: null
            }
        },
        ProfileSettings:{
            screen: ProfileSettings,
            navigationOptions: {
                headerRight: GenericHeader('تنظیمات پروفایل')
            }
        },
        History:{
            screen: OrdersHistoryPage,
            navigationOptions: {
                headerRight: GenericHeader('سابقه سفارشات')
            }
        },
        OrderStatus:{
            screen: OrderStatus,
            navigationOptions: {
                headerRight: GenericHeader('وضعیت سفارشات')
            }
        },
        Login: {
            screen: Login,
            navigationOptions:{
                headerRight: GenericHeader('ورود به ناحیه کاربری')
            },
        },
        SignUp: {
            screen: Signup,
            navigationOptions:{
                headerRight: GenericHeader('ثبت نام')
            },
        },
        ForgotPassword: {
            screen: ForgotPassword,
            navigationOptions:{
                headerRight: GenericHeader('فراموشی رمز عبور')
            },
        },
        ChangePassword: {
            screen: ChangePassword,
            navigationOptions:{
                headerRight: GenericHeader('تغییر رمز عبور')
            },
        }
    },
);

const Tabs = TabNavigator(
  {
    Orders: {
      screen: CheckoutNav,
      navigationOptions:{
        title: 'سبد خرید',
        tabBarIcon: ({ tintColor }) => (
          <Basket tintColor={tintColor} />
        )
      }

    },
    Profile: {
      screen: ProfileNavigator,
      navigationOptions:{
        title: 'ناحیه کاربری',
        tabBarIcon: ({ tintColor }) => (
          <Icon name='ios-contact-outline' style={{color:tintColor}} />
        )
      }

    },
    Search: {
      screen: Search,
      navigationOptions:{
        title: 'جستجو',
        tabBarIcon: ({ tintColor }) => (
          <Icon name='ios-search' style={{color:tintColor}} />
        )
      }

    },
    HomeNav: {
      screen: HomeNavigator,
      navigationOptions:{
        title: 'خانه',
        tabBarIcon: ({ tintColor }) => (
          <Icon name='ios-home' style={{color:tintColor}} />
        )
      }
    }
  },
  {
    // ...TabNavigator.Presets.AndroidTopTabs, //android like tabs in ios
    tabBarComponent: TabBarTop, // indicator in ios
    tabBarPosition: 'bottom',
    tabBarOptions: {
      inactiveTintColor: colors.gray46,
      activeTintColor: colors.base,
      showIcon: true,
      style:{
        backgroundColor: colors.white,
        height:50
      },
      tabStyle:{
        paddingTop:5,
        paddingBottom:0
      },
      labelStyle:{
        padding:0,
        marginTop:2,
        fontSize:10,
        fontFamily: firstFont
      },
      indicatorStyle:{
        top:0,
        backgroundColor:colors.base,
        height:3
      }
    },
    backBehavior: 'none',
    initialRouteName: 'HomeNav',
      lazyLoad: false
  }
)

const DrawerNav = DrawerNavigator(
  {
    Tabs: {
      screen: Tabs,
    },
  },
  {
    contentComponent: props => <ControlPanel {...props} />,
    drawerPosition: 'right'
  }

)
const RootTabs = StackNavigator(
  {
    Drawer: {
        screen: DrawerNav
    }
  },
  {
      navigationOptions:{
          header: null
      }
  }
);

class Application extends React.Component {

    componentDidMount() {
        Linking.addEventListener('url', url => this.handleOpenURL(url.url));
        Linking.getInitialURL().then((url) => {
            this.handleOpenURL(url)
        }).catch(err => {});
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.notificationClickedOrderId !== null && nextProps.notificationClickedOrderId !== this.props.notificationClickedOrderId){
            this.props.actions.changeOrderId(nextProps.notificationClickedOrderId)
        }
    }
    handleOpenURL(url){
        if(url){
            const route = url.replace(/.*?:\/\//g, '');
            const id = route.match(/\/([^\/]+)\/?$/)[1];
            const routeName = route.split('/')[0];
            if (routeName === 'order') {
                this.props.actions.changeOrderId(id)
            }
        }
    }
    render() {
      return (
          <View style={{flex:1}}>
              <RootTabs/>
              <ModalUpdate
                  style={{flex:1}}
                  />
          </View>
      )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        users: state.users,
        orders: state.orders,
        modal: state.modal
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(navActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Application);
