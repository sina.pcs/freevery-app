import axios from 'axios';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    AsyncStorage,
    Platform
} from 'react-native';

import {STORAGE_KEY} from './constants/AppConstants'

storeUser = async (user) => {
    try {
        //await AsyncStorage.multiSet([[STORAGE_KEY_USERNAME, this.state.username], [STORAGE_KEY_mobile, this.state.mobile]]);
        await AsyncStorage.setItem(STORAGE_KEY,user);
    } catch (error) {
        // Error saving data
    }

};
storeToken = async (token) => {
    try {
         await AsyncStorage.setItem('@freevery:token',token);
    } catch (error) {
        // Error saving data
    }

};

removeUser = async (cb) => {
    try {
        await AsyncStorage.removeItem(STORAGE_KEY);
    } catch (error) {

    }
};

checkUser = async () => {
    try {
        let user = await AsyncStorage.getItem(STORAGE_KEY);
        if (user !== null){
            return true;
        }
    } catch (error) {

    }
};

getUserToken= async () => {
    try {
        let user = await AsyncStorage.getItem(STORAGE_KEY);
        if (user !== null){
            user = JSON.parse(user);
            return user.access_token;
        }
    } catch (error) {

    }
};
getUserFromStorage= async () => {
    try {
        let user = await AsyncStorage.getItem(STORAGE_KEY);
        if (user !== null){
            user = JSON.parse(user);
            return user;
        }
    } catch (error) {

    }
};
module.exports = {
    login(mobile, pass, cb) {
        cb = arguments[arguments.length - 1];

        pretendRequest(mobile, pass, (res,token) => {
            if (res.authenticated) {
                storeToken(token).done(function () {
                    storeUser(JSON.stringify(res)).done(function () {
                        if (cb) cb(true);
                    });
                });
                this.onChange(true)
            } else {
                if (cb) cb(false);
                this.onChange(false)
            }
        })
    },
    getToken() {
        return getUserToken().done();
    },
    getUser(){
        return getUserFromStorage().done();
    },
    logout(cb){
        removeUser().done();
        if (cb) cb();
        this.onChange(false)
    },
    loggedIn(cb){
        if(checkUser().done())
        {
            return true;
        }
    },

    onChange() {}
};

function pretendRequest(username, pass, cb) {
    setTimeout(() => {
        axios.post('https://freevery.com/api/web/user/login', {
            headers: {
                'Content-Type': 'application/json'
            },
            username: username,
            password: pass,
            device_type:'mobile'
        })
            .then(function (response) {
                if(response.data.code=='1')
                {
                    let res = response.data.data;
                    res['authenticated'] = true;
                    cb(res,response.data.token);

                }
                else {
                    cb({ authenticated: false })
                }
            })
            .catch(function (error) {
                cb({ authenticated: false })
            });
    }, 0)
}
