import React, { Component } from 'react';
import {MainNavigator} from '../../navigations';
import {addNavigationHelpers} from 'react-navigation';

export default class MainNavComponent extends Component {
    render() {
        return (
          <MainNavigator
            navigation={addNavigationHelpers({ dispatch:this.props.dispatch, state: this.props.nav })} />

        );
    }
}

