import { MainNavigator } from '../../navigations';

const initialNavState = Object.assign({path:''}, MainNavigator.router.getStateForAction(
  {}
));

function mainNavReducer(state = initialNavState, action) {
  let nextState;

  if (action.type.startsWith('Navigation/')) {
      nextState = MainNavigator.router.getStateForAction(action, state);
      nextState.path = MainNavigator.router.getPathAndParamsForState(nextState).path;
      if(nextState.path === state.path) //prevent fast double click
        return state;
  }

  return nextState || state;
}

export default mainNavReducer
