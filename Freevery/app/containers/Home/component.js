
import React, { Component } from 'react';
import {
  FlatList,
  View
} from 'react-native';
import { Content,Button,Icon } from 'native-base';
import CardPost from '../../components/Post/CardPost';
import ListPost from '../../components/Post/ListPost';
import posts from '../../../posts';
const postTypes = ['big','list']

export default class Home extends Component {
  constructor(){
    super();
    this.state = {
      postsType : 0
    }
    this.changePostsType = this.changePostsType.bind(this);
  }

  changePostsType(){

    this.setState((prevState)=>({
      postsType: (prevState.postsType + 1) < postTypes.length ? prevState.postsType + 1 : 0
    }));
  }

  _keyExtractor = (item, index) => item.id;

  render() {
    return (
      <Content>
        {this.state.postsType === 0 &&
        <View>
            <Button transparent onPress={this.changePostsType}>
                <Icon name="ios-list-box" />
            </Button>

            <FlatList
              data={posts}
              keyExtractor={this._keyExtractor}
              renderItem={({item}) => <CardPost key={item.id} post={item}/>}
            />
        </View>
        }
        {this.state.postsType === 1 &&
        <View>
            <Button transparent onPress={this.changePostsType}>
                <Icon name="images" />
            </Button>
            <FlatList
              data={posts}
              keyExtractor={this._keyExtractor}
              renderItem={({item}) => <ListPost key={item.id} post={item}/>}
            />
        </View>
        }
      </Content>
    );
  }
}
