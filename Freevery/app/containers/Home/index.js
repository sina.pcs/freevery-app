import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import Home from './component'

function mapStateToProps(state) {
  return {
    nav: state.mainNavReducer,
  }
}
function mapDispatchToProps(dispatch){
  return Object.assign({dispatch: dispatch}, bindActionCreators({

  }, dispatch));
}
export default connect(
  mapStateToProps,
  mapDispatchToProps

)(Home)
