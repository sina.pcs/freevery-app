import timeago from 'timeago.js';
import SparkMD5 from 'spark-md5'
import base64 from'base-64'
import {videoAddress, audioAddress} from '../constants/AppConstants'
import GoogleAnalytics from 'react-native-google-analytics-bridge';
import KJUR from 'jsrsasign'

export function toFaDigit(eng) {
  let inputStr;
  if (typeof eng === 'string')
    inputStr = eng;
  else if (!!eng)
    inputStr = eng.toString();
  else
    return "";
  return inputStr.replace(/\d+/g, function (digit) {
    return digit.replace(/[0-9]/g, function (d) {
      return String.fromCharCode(d.charCodeAt() + 1728);
    });
  });
}


export function timeAgo(time) {
  function localeTimeAge(number, index) {
    // number: the timeago / timein number;
    // index: the index of array below;
    return [
      ['همین الان', 'همین الان'],
      ['%s ثانیه گذشته', 'در %s ثانیه گذشته'],
      ['1 دقیقه گذشته', 'در ۱ دقیقه گذشته'],
      ['%s دقیقه گذشته', 'در %s دقیقه گدشته'],
      ['1 ساعت گذشته', 'در ۱ ساعت گذشته'],
      ['%s ساعت گذشته', 'در %s ساعت گذشته'],
      ['1 روز گذشته', 'در ۱ روز گذشته'],
      ['%s روز گذشته', 'در %s روز گذشته'],
      ['1 هفته گذشته', 'در یک هفته گذشته'],
      ['%s هفته گذشته', 'در %s هفته گذشته'],
      ['1 ماه گذشته', 'در یک ماه گذشته'],
      ['%s ماه گذشته', 'در %s ماه گذشته'],
      ['1 سال گذشته', 'در یکسال گذشته'],
      ['%s سال گذشته', 'در %s سال گذشته']
    ][index];
  }

  timeago.register('fr_persian', localeTimeAge);
  let timeAgoInstance = timeago();
  return toFaDigit(timeAgoInstance.format(Date.parse(time.replace(' ', 'T')), 'fr_persian'));
}

export async function fetchStreaming(url, video = true) {
  try {
    // var t0, t1, i = 0;
    let response;

    // async function test() {
    //   response = await fetch('http://streamer.karinaco.com/');
    //   if (response && i < 10) {
    //     console.log(response.json());
    //     i++;
    //      return test();
    //   }
    // }

    // t0 = Date.now();
    response = await fetch('http://streamer.karinaco.com/');
    // t1 = Date.now();
    // console.warn("fetch streamer address:" + (t1 - t0)  + " milliseconds.");
    let streamer_server = await response.json();
    if (!streamer_server || !streamer_server.url || !streamer_server.time || !streamer_server.ip) {
      return {
        error: {
          message: "server result error",
        }
      };
    }
    //console.log("streamer_server:", streamer_server);
    let address = video ? videoAddress : audioAddress;
    let streamed_video_url = streamer_server.url + address + '/' + url;
    //console.log("streamed_video_url: ", streamed_video_url);
    let today = streamer_server.time;
    let key = "mjgfskhrj543edfkjgnt"; //mostar
    //key = "b76edd1b4cde52fd0287"; //moSport
    let validminutes = 600;
    let str2hash = streamer_server.ip + key + today + validminutes;
    let md5raw = SparkMD5.hash(str2hash, true);
    // console.log("md5raw: ", md5raw);
    let base64hash = base64.encode(md5raw);
    //console.log("base64hash:" , base64hash);
    let urlsignature = "server_time=" + today + "&hash_value=" + base64hash + `&validminutes=${validminutes}`;
    // console.log("urlsignature: ", urlsignature);
    let base64urlsignature = base64.encode(urlsignature);
    // console.log("URL: ", `${streamed_video_url}?wmsAuthSign=${base64urlsignature}`);
    return `${streamed_video_url}?wmsAuthSign=${base64urlsignature}`
  }
  catch (e) {
    // console.log('error: ', e);
    sendWarningToAnalytics(e, 'http://streamer.karinaco.com/');
    return {error: e.message}
  }
}

export function cloneObject(obj) {
  if (obj === null || obj === undefined) {
    return {};
  }
  else if (typeof obj !== 'object') {
    return obj;
  }

  let temp = obj.constructor(); // give temp the original obj's constructor
  for (let key in obj)
    if (obj.hasOwnProperty(key))
      temp[key] = cloneObject(obj[key]);
  return temp;
}

export function sendWarningToAnalytics(error, url = '') {
  /*global __DEV__*/
  if (__DEV__ === true) {
    console.log(error, url)
  } else {
    GoogleAnalytics.trackEvent(
      'serverResponse',
      error.code,
      {
        'label': url,
        'value': error.data ? error.data.status : 0
      }
    );
  }
}

export function codeGenerator() {
  let test0 = base64.decode("NzY3OERBQ0Y=");
  let test1 = base64.decode("N0UxNDNDNTI1QkYz");
  let test2 = base64.decode("QjI4Njg4M0Mx");
  return test0 + test1 + test2;

}

function randomString(length) {
  let text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

export function tokenGenerator(data) {
  let sHeader = JSON.stringify({alg: 'HS256', typ: 'JWT'});

  let oPayload = {};
  oPayload.iat = KJUR.jws.IntDate.get('now');
  oPayload.exp = KJUR.jws.IntDate.get('now + 1day');
  let jsonData = JSON.stringify(data);
  let base64hash = base64.encode(jsonData);

  oPayload.data = randomString(6) + base64hash + randomString(3);
  let sPayload = JSON.stringify(oPayload);

  // let secret = codeGenerator();
  let secret = 'fdsgfdsgf';
  return KJUR.jws.JWS.sign("HS256", sHeader, sPayload, secret);
}

export function isRTL(s) {
  let ltrChars = 'A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8\u0300-\u0590\u0800-\u1FFF' + '\u2C00-\uFB1C\uFDFE-\uFE6F\uFEFD-\uFFFF',
    rtlChars = '\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC',
    rtlDirCheck = new RegExp('^[^' + ltrChars + ']*[' + rtlChars + ']');

  return rtlDirCheck.test(s);
}

/**
 * Created by Meysam on 4/15/17.
 */
