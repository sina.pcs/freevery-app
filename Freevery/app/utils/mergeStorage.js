import isObject from 'lodash.isobject';
import merge from 'lodash.merge';
import {stores, mergeDefaults} from '../constants/AppConstants'

export default (oldState, newState) => {
  const result = {...oldState};

  for (const key in newState) {
    if (!newState.hasOwnProperty(key)) {
      continue;
    }
    if (stores.indexOf(key) === -1) {
      continue;
    }

    const value = newState[key];

    // Assign if we don't need to merge at all
    if (!result.hasOwnProperty(key)) {
      result[key] = isObject(value) && !Array.isArray(value)
        ? merge({}, value)
        : value;
      continue;
    }

    const oldValue = result[key];

    if (isObject(value) && !Array.isArray(value)) {
      // let mergeDefaults = {};
      // if (!!value.error) {
      //   mergeDefaults.error = null;
      // }
      // if (!!value.isFetching) {
      //   mergeDefaults.isFetching = false;
      // }
      let defaults = {};
      for (let k in mergeDefaults) {
        if (mergeDefaults.hasOwnProperty(k) && !!value[k]) {
          defaults[k] = mergeDefaults[k];
        }
      }
      result[key] = merge({}, oldValue, value, defaults);
    } else {
      result[key] = value;
    }
  }

  return result;
};
