import React, {
  Component
} from 'react'
import {
  View,
  Text,TextInput,
  StyleSheet
} from 'react-native'

import {TabNavigator,TabBarTop,TabBarBottom,StackNavigator} from 'react-navigation';
import Icon from './components/common/Icon'
import { Container, Header, Left, Body, Right, Button, Title, Subtitle } from 'native-base';
import Home from './containers/Home';
import {colors,firstFont} from './constants/AppConstants';
import {generalStyles} from './generalStyles';

class Test extends Component{
  render() {
    return (
      <TextInput
        ref={(c) => this._input = c}
        placeholder={"New Skill"}
        autoFocus={true}
        onChangeText={(text) => {
          this.setState({text})
        }}
        onSubmitEditing={() => {
          this.props.onSubmitEditing(this.state.text);
          this._input.clear();
          this._input.focus();
        }}
      />
    );
  }
}

 const header = {
   headerRight: <Button transparent>
     <Icon name='notifications' style={generalStyles.darkColor} />
   </Button>,
   headerStyle:{
     backgroundColor:'#efefef'
   },
   headerTitleStyle:{
     color: colors.base
   }
 }

const ProfileScreenNavigator = StackNavigator(
  {
    Profile: {
      screen: Test,
      navigationOptions:{
        ...header,headerTitle: 'پروفایل',
      }
    }
  },

  {
    initialRouteName:'Profile'
  }
);

const HomeScreenNavigator = StackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions:{
        ...header,headerTitle: 'Footbin',
      }
    }
  },
  {
    initialRouteName:'Home'
  }
);


export const MainNavigator = TabNavigator(
  {
    TimeLine: {
      screen: Test,
      navigationOptions:{
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-timer" />
        )
      }
    },
    HotLine: {
      screen: Test,
      navigationOptions:{
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-star" />
        )
      }
    },
    Home: {
      screen: HomeScreenNavigator,
      navigationOptions:{
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-football" />
        )
      }
    },
    Search: {
      screen: Test,
      navigationOptions:{
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-search" />
        )
      }
    },

  },
  {
    // ...TabNavigator.Presets.AndroidTopTabs, //android like tabs in ios
    tabBarComponent: TabBarBottom, // indicator in ios
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showLabel: false,
      showIcon: true,
      style:{
        backgroundColor: colors.second
      },
      tabStyle:{
        paddingTop:5,
        paddingBottom:0
      },
      inactiveBackgroundColor: colors.gray85
    },
    backBehavior: 'none',
    initialRouteName: 'Home'
  }
);
