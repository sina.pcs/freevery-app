export const colors = {
  transparent: 'transparent',
  base: '#2c3e50',
  second: '#efefef',
  white: '#ffffff',
  gray13: '#212121',
  gray20: '#333333',
  gray32: '#525252',
  gray46: '#757575',
  gray51: '#828282',
  gray60: '#999999',
  gray70: '#b1b1b1',
  gray76: '#c2c2c2',
  gray81: '#cfcfcf',
  gray85: '#d9d9d9',
  gray89: '#e3e3e3',
  gray90: '#e5e5e5',
  gray93: '#ededed',
  gray95: '#f2f2f2',
  gray96: '#f5f5f5',
  black: '#000000',
  popUpSecond: '#077eff',
  lightOrange: '#ffbb00',
  summerSky: '#4bc6df',
  silver: '#bebebe',
  lightBlue: '#1570a6',
  Seagull: '#7fb1ce',
  whiteSmoke: '#EAEAEA',
  dodgerBlue: '#1E90FF',
  errorRed: '#f53e3e',
  warning: '#FFC107',
  warningBorder: '#e5be1c',
  green: '#008000',
  greenDone: '#8BC34A',
  pink: '#ed4d83',
  premium: '#f8941d'
};
export const STORAGE_KEY = '@freevery:user';
export const stores = [
  'categories',
  'posts',
  'profile',
  //'profileNavReducer',
  // 'comments',
  'recentlyRead',
  'bookmarks',
  'relatedPosts',
  'slides',
  //'poll'
  'update'
];
export const mergeDefaults = {
  error: null,
  isFetching: false,
  allFetchedOld: false,
  sendResult: null,
  registerUserResult: null,
  getAllBookmarks: {
    pageNumber: 0,
    lastPage: false
  }
};
export const videoAddress = 'mostar';
export const audioAddress = 'mostaraudio';
export const baseUrl = 'http://mostar.ir/api/';
export const requestUrl =
  // 'http://mostar.ir/api/v3/'; //server
  baseUrl + 'v4/';
export const likeDislikeUrl = baseUrl + 'plusone/v1/';
export const pollUrl = baseUrl + 'totalpoll/v1/';
export const updateUrlAndroid = 'http://app.karinaco.com/app/android/mostar.json';
export const updateUrlIos = 'http://app.karinaco.com/app/android/mostar.json';
export const requestUrlAd = 'http://adsho.ir/www/delivery/json.php?zoneid=139';
export const smsLoginUlr = baseUrl+ 'ar/v1/';
// 'http://7sobh.com/wp-api/v2/'; //server
// 'http://172.16.0.194/wp-api/v2/'; //dev-server
export const firstFont = 'IRANSansMobile';
export const iconFont = 'icomoon';
export const fetchInterval = 5 * 1000;//interval in milliseconds
export const cleanPostsInterval = 3 * 86400 * 1000;//interval in milliseconds
export const topBarHeight = 64;
export const bottomBarHeight = 46;
export const commentsPaginationLength = 10;
export const postPaginationLength = 10;
export const headerButtonWidth = 40;