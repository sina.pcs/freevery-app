export const loadState = () => {
    try{
        const serializedState = localStorage.getItem('jamseparState');
        if(serializedState === null)
            return undefined;

        return JSON.parse(serializedState);

    }
    catch (err){
        return undefined;
    }
};

export const saveState = (state) => {
    try{
        const serializedState = JSON.stringify(state);
        localStorage.setItem('jamseparState',serializedState);
    }
    catch (err){
        //ignore errors
    }
};