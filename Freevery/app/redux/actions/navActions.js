import * as types from './actionTypes';

export function changeOrderId(id) {
    return{
        type:types.CHANGE_ORDER_ID,
        id
    }
}

export function resetOrderId(){
    return{
        type: types.CHANGE_ORDER_ID,
        id: null
    }
}