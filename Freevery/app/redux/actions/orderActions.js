import * as types from './actionTypes';

export function addOrder(order) {
    return{
        type:types.ADD_ORDER,
        order
    }
}

export function updateOrder(order) {
    return{
        type:types.UPDATE_ORDER,
        order
    }
}

export function increaseOrder(orderIndex) {
    return{
        type:types.INCREASE_ORDER,
        orderIndex
    }
}

export function decreaseOrder(orderIndex) {
    return{
        type:types.DECREASE_ORDER,
        orderIndex
    }
}

export function deleteOrder(orderIndex) {
    return{
        type:types.DELETE_ORDER,
        orderIndex
    }
}

export function increaseTotal(amount) {
    return{
        type:types.INCREASE_TOTAL,
        amount
    }
}

export function decreaseTotal(amount) {
    return{
        type:types.DECREASE_TOTAL,
        amount
    }
}

export function clearOrder() {
    return{
        type: types.CLEAN_ORDER
    }
}