import * as types from './actionTypes';

export function addUser(user) {
    return{
        type:types.ADD_USER,
        user
    }
}

export function updateUser(user) {
    return{
        type:types.UPDATE_USER,
        user
    }
}

export function removeUser() {
    return{
        type:types.REMOVE_USER,
        
    }
}