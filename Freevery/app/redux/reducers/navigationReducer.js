import * as types from '../actions/actionTypes';

const initialState = {
    orderId: null
};
export default function navReducer(state=initialState,action) {

    switch (action.type) {
        case types.CHANGE_ORDER_ID:
            return {...state,
                orderId: action.id};
        default:
            return state;
    }
}
