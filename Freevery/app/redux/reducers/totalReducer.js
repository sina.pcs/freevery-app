import * as types from '../actions/actionTypes';

export default function totalReducer(state=0,action) {
    switch (action.type) {
        case types.INCREASE_TOTAL:
            return state + action.amount;

        case types.DECREASE_TOTAL:
            return state - action.amount;

        case types.CLEAN_ORDER:
            return 0;

        default:
            return state;
    }
}
