import * as types from '../actions/actionTypes';

export default function orderReducer(state=[],action) {
    switch (action.type) {
        case types.ADD_ORDER:
            return [...state,
                Object.assign({},action.order)
            ];

        case types.UPDATE_ORDER:
            // return { ...state,
            //     [0]: action.order
            // };
            let orderIndex = state.findIndex(order => order.pid == action.order.pid);
            return [
                ...state.slice(0, orderIndex),
                action.order,
                ...state.slice(orderIndex + 1)
            ];

        case types.INCREASE_ORDER:
            let newOrders = state.slice();
            let oldOrder = newOrders[action.orderIndex];
            newOrders[action.orderIndex] =  Object.assign({}, oldOrder, { quantity: oldOrder.quantity + 1 });
            return newOrders;
            // return state.map((order,index) => (index==action.orderIndex) ? {...order, quantity: order.quantity+1} : order);

        case types.DECREASE_ORDER:
            newOrders = state.slice();
            oldOrder = newOrders[action.orderIndex];
            newOrders[action.orderIndex] = Object.assign({}, oldOrder, {quantity: oldOrder.quantity - 1});
            return newOrders;

        case types.DELETE_ORDER:
            return [...state.slice(0, action.orderIndex),
                ...state.slice(action.orderIndex + 1)];

        case types.CLEAN_ORDER:
            return [];

        default:
            return state;
    }
}
