import * as types from '../actions/actionTypes';

export default function userReducer(state=[],action) {
    switch (action.type) {
        case types.ADD_USER:
            return [...state,
                Object.assign({},action.user)
            ];

        case types.UPDATE_USER:
            let newUsers = state.slice();
            let oldUser = newUsers[0];
            newUsers[0] = Object.assign({}, action.user);
            return newUsers;

        case types.REMOVE_USER:
            return [...state.slice(0, 0)];

        default:
            return state;
    }
}
