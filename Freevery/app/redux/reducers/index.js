import {combineReducers} from 'redux';
import users from './userReducer';
import orders from './orderReducer';
import total from './totalReducer';
import nav from './navigationReducer'
const rootReducer = combineReducers({
   users,orders,total,nav
});

export default rootReducer;