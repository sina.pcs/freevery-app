import { StyleSheet, PixelRatio,Platform } from 'react-native';
const deviceScreen = require('Dimensions').get('window')
import {colors,firstFont} from './constants/AppConstants';
module.exports = {
    rtl:{
        textAlign:'right'
    },
  scrollView: {
    backgroundColor: '#B99BC4',
  },
  Row:{display:'flex',flexDirection:'row'},
  Column:{display:'flex',flexDirection:'column'},
  container: {
    backgroundColor:colors.second
  },
  controlPanel: {
    flex: 1,
    backgroundColor:'#fbfbfb',
    borderLeftWidth: 0.5,
    borderRightWidth: 0.5,
    borderColor: '#d8d7d7',
  },
  controlPanelText: {
    color:'white',
  },
  controlPanelWelcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 25,
    color:'white',
    fontWeight:'bold',
  },
  categoryLabel: {
    fontSize: 15,
    textAlign: 'left',
    left: 10,
    padding:10,
    fontWeight:'bold',
  },
  listItem:{
    marginLeft:0,
    paddingLeft:0,
      paddingRight:0,
    borderColor: '#eeeeee',
    justifyContent:'flex-end',
  },
  row: {
    flexDirection: 'row',
    backgroundColor:'white',
    borderRadius: 0,
    borderWidth: 0,
    padding:0,
    borderTopWidth: 1 / PixelRatio.get(),
    borderColor: '#d6d7da',
    padding:10,
    alignItems: 'center'
  },
  lastRow: {
    flexDirection: 'row',
    backgroundColor:'white',
    borderRadius: 0,
    borderWidth: 0,
    padding:0,
    borderTopWidth: 1 / PixelRatio.get(),
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: '#d6d7da',
    padding:10,
    alignItems: 'center'
  },
  rowLabel: {
    left:10,
    fontSize:15,
    flex:1,
  },
  rowInput: {
    right:10,
  },
  sliderMetric: {
    right:10,
    width:30,
  },
  slider: {
    width: 150,
    height: 30,
    margin: 10,
  },
  picker: {
    backgroundColor:'white',
    borderRadius: 0,
    borderWidth: 0,
    padding:0,
    borderBottomWidth: 1 / PixelRatio.get(),
    borderTopWidth: 1 / PixelRatio.get(),
    borderColor: '#d6d7da',
  },
  label: {
    fontSize: 20,
    textAlign: 'left',
    margin: 0,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
    text:{
      fontFamily:firstFont
    },
    button:{
        backgroundColor:colors.white,
        borderWidth:1,
        borderColor:colors.base,
        padding:10,
        marginVertical:10,
        borderRadius:50,
        flexDirection:'row',
        justifyContent:'center',
        elevation:3
    },
  buttonText:{
    paddingRight:10, fontSize:14, fontFamily:firstFont,textAlign:'center',paddingLeft:10
  },
    buttonIcon:{ color: colors.base,fontSize:24 },
    input:{marginVertical: 10,paddingRight:10,backgroundColor:colors.white},
    inputIcon:{ color: colors.base,fontSize:24 },
    errorBox:{
        paddingHorizontal:20,
        paddingVertical:10,
        marginBottom:20,
        backgroundColor:'white',
        borderRadius:50,
        flexDirection:'row'
    },
    errorText:{
        color: 'red',
        fontFamily:firstFont,
        textAlign:'center',
        marginRight:10
    },
    errorIcon:{
      color:'red',
        fontSize:25
    },

  panelTitle: {
    borderBottomWidth:1/PixelRatio.get(),
    borderBottomColor: '#aaa',
    backgroundColor:'#fff',
    padding:10,
    fontSize:16,
    textAlign:'center',
    color:'#34a853'
  }
};
