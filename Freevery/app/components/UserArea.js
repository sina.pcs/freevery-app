import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Image,
    ScrollView,
    Modal,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';
import {
    Icon,
    ListItem,
    List,
    Col,
    Container,
    Content
} from 'native-base';
import {colors,firstFont} from '../constants/AppConstants'
import Text from './Text'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import styles from '../styles';
import Login from './Login';
import { Actions } from 'react-native-router-flux';
import auth from '../auth';
import Button from "./Button";

import {STORAGE_KEY} from '../constants/AppConstants'

class UserArea extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loggedIn:false,users:[]
        };
    }
    render() {
        if(this.props.users.length) {
            let user = this.props.users[0];
            return (
                <ScrollView style={{flex:1,backgroundColor:colors.base}}>
                        <View style={{backgroundColor:colors.base,justifyContent:'center',alignItems:'center',paddingVertical:30,borderBottomWidth:1,borderBottomColor:colors.white}}>
                            <Image source={require('../assets/app-logo.png')} />
                            <Text style={{fontFamily:firstFont,fontSize:18,textAlign:'center',color:colors.white}}>{user.first_name?user.first_name:user.username}</Text>
                            <Text style={{fontFamily:firstFont,fontSize:18,textAlign:'center',color:colors.white}}>{user.balance?user.balance:0} ریال</Text>
                        </View>
                        <View style={{paddingVertical:50,paddingHorizontal:30,backgroundColor:colors.base}}>
                            <Button onPress={() => this.props.navigation.navigate('ProfileSettings')}
                                    title="تنظیمات پروفایل"
                                    icon="ios-settings"
                            />
                            <Button onPress={() => this.props.navigation.navigate('History')}
                                    title="سابقه سفارشات"
                                    icon="ios-archive"
                            />
                            <Button onPress={() => this.props.navigation.navigate('Deposit')}
                                    title="افزایش موجودی"
                                    icon="ios-add-circle"
                            />
                            <Button onPress={() => this.props.navigation.navigate('ChangePassword')}
                                    title="تغییر کلمه عبور"
                                    icon="ios-lock"
                            />
                            <Button onPress={() => auth.logout(()=>{this.props.actions.removeUser()})}
                                    title="خروج"
                                    icon="ios-log-out"
                            />
                        </View>
                </ScrollView>
            );
        }
        else{
            return(
                <ScrollView style={{flex:1,backgroundColor:colors.base}}>
                    <View style={{backgroundColor:colors.base,justifyContent:'center',alignItems:'center',paddingVertical:50,borderBottomWidth:1,borderBottomColor:colors.white}}>
                        <Image source={require('../assets/app-logo.png')} />
                        <Text style={{fontFamily:firstFont,fontSize:18,textAlign:'center',color:colors.white}}>ناحیه کاربری</Text>
                    </View>
                    <View style={{paddingVertical:50,paddingHorizontal:30,backgroundColor:colors.base}}>
                            <Text style={{fontFamily:firstFont,textAlign:'center',color:colors.white,fontSize:14,paddingBottom:20}}>
                                برای انجام سفارشات باید ثبت نام کرده و وارد ناحیه کاربری خود شوید
                            </Text>
                        <Button onPress={() => this.props.navigation.navigate('Login',{from:'menu'})}
                                title="ورود"
                                icon="ios-log-in"
                        />
                        <Button onPress={() => this.props.navigation.navigate('SignUp',{from:'menu'})}
                                title="ثبت نام"
                                icon="ios-person-add"
                        />
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                            <View>
                                <Text style={{color:colors.white, textAlign:'center',paddingTop:20}}>
                                    رمز عبور خود را فراموش کرده اید؟
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            );
        }
    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserArea)