import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Modal,
    AsyncStorage,
    TouchableHighlight
} from 'react-native';
import {
    Icon,
    ListItem,
    List,
    Col,
    Container,
    Content,
    Left,
    Right,
    Body
} from 'native-base';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import styles from '../styles';
import Login from './Login';
import { Actions } from 'react-native-router-flux';
import auth from '../auth';
import Button from "./Button";
import Text from './Text';
import categoriesData from '../data/navigationMenu.json';

import {STORAGE_KEY} from '../constants/AppConstants'

const styles2 = { 
  listItem:{
    marginLeft:0,
    paddingLeft:100,
      paddingRight:100,
    borderColor: '#000',
  },
};
class ControlPanel extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loggedIn:false,users:[],
            showSubMenu:[false,false]
        };
        this.loadMenu = this.loadMenu.bind(this);
    }
    navigate(id,title){
        // Actions.refresh({key: 'drawer', open: value => !value });
        // setTimeout(() => Actions.home({cid:id}));
      this.props.navigation.navigate('HomeScreen',{
        cid: id,
        title
      })

    }
    showSubMenu(i,val){
        let subMenu = [false,false];
        subMenu[i] = val;
        this.setState({showSubMenu:subMenu})
    }

    loadMenu(){
            let navigation = categoriesData.map((category,i)=>{

                let subNavs = category.subCategories.map((subCategory)=>{

                    let links = subCategory.hasChild?subCategory.subCategories.map((link) => {
                        let url = 'category/'+link.id;
                        return (
                        <ListItem button onPress={() => {this.navigate(link.id,link.title)}} style={styles.listItem} key={'category'+link.id}>
                            <Text style={{paddingRight:10}}>{link.title}</Text>
                        </ListItem>
                        );
                    }): <Text></Text>;

                    let url = 'category/'+subCategory.id;
                    //let subCategoryHeader = subCategory.hasChild?<h3>{subCategory.title}</h3>:<h3><Link to={url}>{subCategory.title}</Link></h3>;
                    return (

                        <View style={{backgroundColor:'#eeeeee'}} key={'category'+subCategory.id}>
                            {links}
                        </View>
                    )
                });
                return (
                <View key={category.id}>
                    <ListItem icon
                              onPress={()=>this.showSubMenu(i,!this.state.showSubMenu[i])}
                              key={'category'+category.id}
                              style={{...styles.listItem,...{backgroundColor:this.state.showSubMenu[i]?'#ddd':'transparent'}}}>
                        <Body style={{paddingLeft:10}}>
                        {this.state.showSubMenu[i] ?
                            <Icon name="ios-arrow-up-outline" style={{color: '#0A69FE',fontSize:15}}/>
                            :
                            <Icon name="ios-arrow-down-outline" style={{color: '#0A69FE',fontSize:15}}/>
                        }
                        </Body>
                        <Right style={{marginRight:0,paddingRight:10}}>
                            <Text style={{paddingRight:0, fontSize:15}}>{category.title}</Text>
                        </Right>
                    </ListItem>

                    {this.state.showSubMenu[i] &&
                        subNavs
                    }
                </View>
                )
            });
            return(
                <Container>
                    <Content>
                        <Text style={{fontSize:22,padding:15,textAlign:'center',backgroundColor:'#2c3e50',width:'100%',color:'#fff'}}>دسته بندی محصولات</Text>
                        {navigation}
                    </Content>
                </Container>
            );
    }
    render() {
            return (
                <View style={styles.controlPanel}>
                    {this.loadMenu()}
                </View>
            )
    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel)