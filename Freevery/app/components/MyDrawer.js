import React, { Component } from 'react';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import MyControlPanel from './ControlPanel';
import {BackHandler} from 'react-native';

class MyDrawer extends Component {

    constructor(props, context) {
        super(props, context);
        this.closeDrawer = this.closeDrawer.bind(this);
    }

    closeDrawer(){
        if(this.drawer._open)
        this.drawer.close();
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', () => {
            //Actions.refresh({key: 'drawer', open: false });
            if(this.drawer._open){
                this.closeDrawer();
                return true;
            }
        });
    }
    render(){
        const state = this.props.navigationState;
        const children = state.children;
        var controlPanel = <MyControlPanel closeDrawer={() => {
            this.closeDrawer();
        }} />;
        const drawerStyles = {
            drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3, backgroundColor:'#fff'},
        };
        return (
            <Drawer
                ref={(ref)=>{this.drawer = ref}}
                side='right'
                closedDrawerOffset={-3}
                open={state.open}
                onOpen={()=>Actions.refresh({key:state.key, open: true})}
                onClose={()=>Actions.refresh({key:state.key, open: false})}
                type="overlay"
                content={controlPanel}
                tapToClose={true}
                openDrawerOffset={0}
                panCloseMask={0}
                negotiatePan={true}
                tweenHandler={(ratio) => ({
                    main: { opacity:Math.max(0.54,1-ratio) }
                })}

                tweenDuration={200}
                styles={drawerStyles}
            >
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
}


export default MyDrawer; 