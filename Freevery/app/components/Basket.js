'use strict';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {Icon} from 'native-base';
import {Text,View} from 'react-native';



class Basket extends React.Component{
  render(){

    let totalNumberOfOrders = 0;
    this.props.orders.map(
      (order, index) => {
        totalNumberOfOrders += order.quantity;
      }
    );

    return(
      <View>
        <Text style={{position:'absolute',right:0,top:0,padding:2,backgroundColor:'#ff0000',fontSize:12,zIndex:1,color:'#fff',fontWeight:'bold',borderRadius:100}}>{totalNumberOfOrders}</Text>
        <Icon name='ios-basket' style={{color:this.props.tintColor}} />
      </View>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    users: state.users,
    orders: state.orders
  }
}

export default connect(mapStateToProps)(Basket);