import React, {Component} from 'react';
import {connect} from 'react-redux';
import OneOrderInList from './OneOrderInList';
import {bindActionCreators} from 'redux';
import * as orderActions from '../redux/actions/orderActions';
import axios from 'axios';
import {
    SwitchIOS,
    View,
    AsyncStorage,
    Linking,
    ScrollView,
    StyleSheet
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col,
    List,
    ListItem,
    Radio,
    H3
} from 'native-base';
import Modal from './Modal';
import styles from '../styles';
import PushNotification from 'react-native-push-notification';
import Text from './Text';
import DeviceInfo from 'react-native-device-info';
import {colors, firstFont, iconFont} from "../constants/AppConstants";

let jalaali = require('jalaali-js');

class OrderStatus extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            order: null,
            orderCancelDialog: false,
            rows: []
        }
        this.getOrderById = this.getOrderById.bind(this);
        this.pushConnect = this.pushConnect.bind(this);
    }

    componentDidMount() {
        this.getOrderById(this.props.navigation.state.params.id);
    }

    componentWillReceiveProps(nextProps) {
        this.getOrderById(nextProps.navigation.state.params.id);
    }

    getUser = async () => {
        try {
            let user = await AsyncStorage.getItem('@freevery:user');
            if (user !== null) {
                user = JSON.parse(user);
            }
            return user;
        } catch (error) {
            return null;
        }
    };

    getDevice = async () => {
        try {
            let device = await AsyncStorage.getItem('@freevery:device');
            if (device !== null) {
                device = JSON.parse(device);
            }
            return device;
        } catch (error) {
            return false;
        }
    };

    storeDevice = async (device) => {
        try {
            await AsyncStorage.setItem('@freevery:device', device);
        } catch (error) {

        }
    };

    pushConnect() {
        let _this = this;
        this.getUser().done(user => {
            PushNotification.configure({
                // (optional) Called when Token is generated (iOS and Android)
                onRegister: function (token) {
                    _this.getDevice().done((device) => {
                        if (!device || device.game_version != DeviceInfo.getVersion() || device.token != token.token || !device.id || (user && device.user_id != user.id)) {
                            let objectToSend = {
                                identifier: token.token,
                                device_type: "1",
                                game_version: DeviceInfo.getVersion(),
                                device_model: DeviceInfo.getModel(),
                                device_os: DeviceInfo.getSystemName() + DeviceInfo.getSystemVersion(),
                                sdk: DeviceInfo.getUserAgent(),
                                user_id: user ? user.id : ''
                            };

                            //update token
                            if (device && (device.token !== token.token || device.game_version != DeviceInfo.getVersion())) {
                                objectToSend.previous_token = device.token;
                            }
                            axios.post('https://freevery.com/api/web/push/register-token',
                                objectToSend
                            )
                                .then(function (result) {
                                    if (result.data.code == '1') {
                                        let id = JSON.parse(result.data.data).id;
                                        let device = {
                                            token: token.token,
                                            id: id,
                                            game_version: DeviceInfo.getVersion(),
                                            user_id: user.id
                                        };
                                        _this.storeDevice(JSON.stringify(device)).done(() => {
                                            console.log(device);
                                        });
                                    }
                                })
                                .catch(function (error) {
                                })

                        }
                    });

                }
            });
        });
    }

    getToken = async () => {
        try {
            let token = await AsyncStorage.getItem('@freevery:token');
            return token;
        } catch (error) {
            return false;
        }
    };

    getOrderById(orderId) {
        this.setState({order: null});
        let _this = this;
        this.getToken().done(function (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            axios.get('https://freevery.com/api/web/orders/get-by-orderid?order_id=' + orderId)
                .then(function (response) {
                    if (response.data.code === "1") {
                        _this.setState(
                            {
                                order: response.data.data[0]
                            }
                        );
                        if (response.data.data[0].state != "7")
                            _this.pushConnect();
                    }
                })
                .catch(function (error) {
                    if (error.response.data.status == "401") {
                        _this.Actions.logout();
                    }
                    else
                        _this.getOrderById(orderId);
                });
        });
    }
    cancelOrder = () => {
        const orderDetail = this.state.order.orderDetail;
        let orderId = this.state.order.id;
        this.setState({
            order:null,
            orderCancelDialog: false
        });
        let _this = this;
        this.getToken().done(function (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            axios.put('https://freevery.com/api/web/orders/cancel-order-by-user?order_id=' + orderId)
                .then(function (response) {
                    if (response.data.code = "1") {
                        _this.setState(
                            {
                                order: {...response.data.data[0],orderDetail}
                            }
                        )
                    }
                })
                .catch(function (error) {
                    if(error.response.data.status == "401")
                    {
                        // _this.props.router.replace('/logout');
                    }
                    else
                        _this.cancelOrder();
                });
        });
    };

    resetOrder = () => {
        const orderDetail = this.state.order.orderDetail;
        let orderId = this.state.order.id;
        this.setState({
            order:null,
            orderCancelDialog: false
        });
        let _this = this;
        this.getToken().done(function (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            axios.put('https://freevery.com/api/web/orders/reset-order-by-user?order_id=' + orderId)
                .then(function (response) {
                    if (response.data.code = "1") {
                        _this.setState(
                            {
                                order: {...response.data.data[0],orderDetail}
                            }
                        )
                    }
                })
                .catch(function (error) {
                    if(error.response.data.status == "401")
                    {
                        // _this.props.router.replace('/logout');
                    }
                    else
                        _this.cancelOrder();
                });
        });
    };

    confirmOrder = (orderId) => {
        const orderDetail = this.state.order.orderDetail;
        this.setState({
            order: null
        });
        this.getToken().done((token) => {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            axios.put('https://freevery.com/api/web/orders/confirm-delivery-by-user?order_id=' + orderId)
                .then((response) => {
                    if (response.data.code = "1") {
                        this.setState(
                            {
                                order: {...response.data.data[0],orderDetail}
                            }
                        )
                    }
                })
                .catch(function (error) {
                    if (error.response.data.status == "401") {
                        //logout
                    }
                    else
                        this.confirmOrder();
                });
        });
    };

    goToPayment(orderId) {
        let url = 'https://freevery.com/api/payment/index.php?order_id=' + orderId + '&user_agent=android';
        Linking.canOpenURL(url).then(supported => {
            if (!supported) {
                alert('دستگاه شما این قابلیت را پشتیبانی نمیکند. لطفا از وب سایت freevery.com سفارش خود را پرداخت کنید.')
            } else {
                return Linking.openURL(url);
            }
        }).catch(err => alert('دستگاه شما این قابلیت را پشتیبانی نمیکند. لطفا از وب سایت freevery.com سفارش خود را پرداخت کنید.'));
    }

    render() {
        let modalContent = <View/>;
        if (!this.state.order) {
            return (
                <Container style={styles.container}>
                    <Content>
                        <View style={{alignItems: 'center'}}>
                            <Spinner/>
                        </View>
                    </Content>
                </Container>
            )
        }
        else {
            let paymentMethod = '';
            switch (this.state.order.payment_method) {
                case '1':
                    paymentMethod = 'پرداخت آنلاین';
                    break;
                case '2':
                    paymentMethod = 'پرداخت نقدی درب منزل';
                    break;
                case '3':
                    paymentMethod = 'پرداخت از طریق درگاه POS';
                    break;
            }

            let products = this.state.order.orderDetail.map((product, index) =>
                <View style={{display: 'flex', flexDirection: 'row', padding: 10}} key={index}>
                    <Text style={{flex: 1, textAlign: 'center'}}>
                        {product.price}
                    </Text>
                    <Text style={{flex: 1, textAlign: 'center'}}>
                        {product.count}
                    </Text>
                    <Text style={{flex: 2}}>
                        {product.product_title}
                    </Text>
                </View>
            );

            let changedProducts = this.state.order.state == '0' ?
                <View></View> : this.state.order.orderDetail.map((product, index) => {
                        let bgcolor = '';
                        switch (product.state) {
                            case '1'://edited
                                bgcolor = '#afdeaf';
                                break;
                        }
                        return (
                            <View style={{display: 'flex', flexDirection: 'row', backgroundColor: bgcolor, padding: 10}}
                                  key={'changed' + index}>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    {product.confirmed_price}
                                </Text>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    {product.confirmed_count}
                                </Text>
                                <Text style={{flex: 2}}>
                                    {product.confirmed_product_title}
                                </Text>
                                {product.state == 2 && //deleted
                                <View style={{
                                    height: 1,
                                    width: '100%',
                                    backgroundColor: '#333',
                                    position: 'absolute',
                                    top: '100%',
                                    right: 10
                                }}>

                                </View>
                                }
                            </View>

                        );
                    }
                );
            let orderStatus = <Text></Text>;
            let headerText = <Text></Text>;
            let button = null;
            let cancelButton = null;
            debugger;
            switch (this.state.order.state) {
                case '0':
                    headerText = `با تشکر از شما. سفارش ثبت شد و در انتظار تایید از طرف تامین کنندگان ما است. به زودی و پس از تایید اطلاع رسانی خواهد شد.`
                    orderStatus = 'در انتظار تایید سوپرمارکت'
                    break;
                case '1':
                    if (this.state.order.payment_method == "1") {
                        headerText = `سفارش شما تایید شد و پس از پرداخت آنلاین ارسال خواهد شد.`;
                        orderStatus = 'در انتظار پرداخت آنلاین';
                        button = <Button
                            onPress={() => this.goToPayment(this.state.order.id)}
                            block success large style={{width: '100%'}}>
                            <Text style={[styles.buttonText, {color: 'white'}]}>پرداخت آنلاین</Text>
                        </Button>;
                    }
                    else {
                        headerText = 'سفارش شما تایید شد و برای شما ارسال خواهد شد';
                        orderStatus = 'سفارش شما تایید شد';
                    }
                    break;
                case '2':
                    if (this.state.order.payment_method == "1") {
                        headerText = 'سفارش شما با اندکی تغییر توسط سوپر مارکت تایید شده است. در صورت تایید تغییرات روی دکمه تایید سفارش کلیک کنید.';
                        orderStatus = 'در انتظار تایید شما و پرداخت آنلاین';
                        button = <Button
                            onPress={() => this.goToPayment(this.state.order.id)}
                            block success large style={{flex:1}}>
                            <Text style={[styles.buttonText, {color: 'white'}]}>تایید تغییرات و پرداخت آنلاین</Text>
                        </Button>;
                    }
                    else {
                        headerText = 'سفارش شما با اندکی تغییر توسط سوپر مارکت تایید شده است. در صورت تایید تغییرات روی دکمه تایید سفارش کلیک کنید.';
                        orderStatus = 'در انتظار تایید شما';
                        button = <Button
                            onPress={() => this.confirmOrder(this.state.order.id)}
                            block success large style={{flex:1}}>
                            <Text style={[styles.buttonText, {color: 'white'}]}>تایید تغییرات</Text>
                        </Button>;
                    }
                    cancelButton = <Button
                        onPress={() => {
                            this.setState({
                                orderCancelDialog: true
                            })
                        }}
                        block danger large  style={{flex:1}}>
                        <Text style={[styles.buttonText, {color: 'white'}]}>عدم تایید تغییرات</Text>
                    </Button>;
                    break;
                case '3':
                    orderStatus = 'پرداخت ناموفق';
                    button = <Button
                        onPress={() => this.goToPayment(this.state.order.id)}
                        block success large style={{width: '100%'}}>
                        <Text style={[styles.buttonText, {color: 'white'}]}>پرداخت مجدد</Text>
                    </Button>;
                    break;
                case '4':
                    if(this.state.order.payment_method == "1"){
                        headerText = ` سفارش شما به زودی ارسال خواهد شد.`;
                        orderStatus = 'پرداخت موفق';
                    }
                    else{
                        headerText = ` سفارش شما به زودی ارسال خواهد شد.`;
                        orderStatus = 'تایید شده توسط شما';
                    }
                    break;
                case '5':
                    orderStatus = 'در راه';
                    button = <View style={{textAlign:'center',marginTop:0,border:'2px solid #ddd'}}>
                        <Text style={{background:'#ddd',textAlign:'center',fontSize:20,padding:10,margin:0}}>{this.state.order.pincode}</Text>
                        <Text style={{textAlign:'center'}}>
                            لطفا این کد را پس از دریافت سفارش به نماینده سوپرمارکت تحویل دهید
                        </Text>
                    </View>;
                    break;
                case '6':
                    orderStatus = 'تحویل داده شد';
                    break;
                case '7':
                    orderStatus = 'بسته شد';
                    break;
                case '-1':
                    orderStatus = 'لغو شده';
                    break;

            }

            let t = this.state.order.insert_datetime.split(/[- :]/);
            let j = jalaali.toJalaali(parseInt(t[0]), parseInt(t[1]), parseInt(t[2]));

            modalContent = <View>
                    <View style={modalStyles.header}>
                        <View style={modalStyles.header1}>
                            <View style={modalStyles.rightHeader}>
                                <Text style={modalStyles.headerText}>عدم تایید / لغو</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView style={modalStyles.messageContainer}>
                        <Text>
                            در صورتی که می خواهید سفارش شما به صورت کامل لغو شود بر روی دکمه لغو کامل سفارش کلیک کنید.
                        </Text>
                        <Button onPress={this.cancelOrder} danger block style={{marginTop:10}}>
                            <Text style={[styles.buttonText, {color: 'white'}]}>لغو کامل سفارش</Text></Button>
                        <Text style={{marginTop: 20}}>
                            در صورتی که تغییرات صورت گرفته را نمی پذیرید بر روی این دکمه کلیک کنید تا برای بررسی مجدد
                            ارسال
                            شود.
                        </Text>
                        <Button onPress={this.resetOrder} warning block style={{marginTop:10}}>
                            <Text style={[styles.buttonText, {color: 'white'}]}>عدم تایید تغییرات</Text>
                        </Button>
                    </ScrollView>
                </View>;

            return (
                <Container style={{backgroundColor: 'white'}}>
                    <Modal
                        open={this.state.orderCancelDialog}
                        modalStyle={modalStyles.modal}
                        ref="modal"
                        modalDidClose={()=>this.setState({orderCancelDialog: false})}
                        closeOnTouchOutside={true}
                    >
                        {modalContent}
                    </Modal>
                    <Content>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{textAlign: 'center', padding: 10}}>
                                ساعت: {t[3] + ':' + t[4]}
                            </Text>
                            <Text style={{textAlign: 'center', padding: 10}}>
                                تاریخ: {j.jy + '/' + j.jm + '/' + j.jd}
                            </Text>
                        </View>
                        <View style={{padding: 10, borderWidth: 1, borderColor: '#ddd', margin: 10}}>
                            <Text>نام: {this.state.order.customer_fullname}</Text>
                            <Text>آدرس: {this.state.order.area + ' - ' + this.state.order.customer_address}</Text>
                            <Text>شماره تماس: {this.state.order.customer_mobile}</Text>
                            <Text>نوع پرداخت: {paymentMethod}</Text>
                        </View>
                        <Text style={{textAlign: 'center', padding: 10}}>
                            {headerText}
                        </Text>
                        <Text style={{textAlign: 'center', color: '#00ff00', margin: 10, fontSize: 20}}>
                            {orderStatus}
                        </Text>

                        <View>
                            <Text style={{
                                textAlign: 'center',
                                backgroundColor: '#ddd',
                                padding: 10,
                                fontSize: 15,
                                marginTop: 10
                            }}>
                                سفارش اصلی
                            </Text>
                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 10, padding: 10}}>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    قیمت واحد
                                </Text>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    تعداد
                                </Text>
                                <Text style={{flex: 2}}>
                                    نام محصول
                                </Text>
                            </View>
                            {products}
                        </View>
                        {this.state.order.state != '0' &&
                        <View style={{paddingBottom: 50}}>
                            <Text style={{
                                textAlign: 'center',
                                backgroundColor: '#ddd',
                                padding: 10,
                                fontSize: 15,
                                marginTop: 10
                            }}>
                                سفارش پذیرفته شده
                            </Text>
                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 10, padding: 10}}>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    قیمت واحد
                                </Text>
                                <Text style={{flex: 1, textAlign: 'center'}}>
                                    تعداد
                                </Text>
                                <Text style={{flex: 2}}>
                                    نام محصول
                                </Text>
                            </View>
                            {changedProducts}
                        </View>
                        }
                    </Content>

                    {this.state.order.state == '5' && <Footer style={{padding:10,height:150,backgroundColor:'#DFF0D8'}}>
                        {button}
                    </Footer>}
                    {this.state.order.state != '5' && !this.state.orderCancelDialog && (button || cancelButton) && <Footer style={{backgroundColor: '#fff', flexDirection: 'row'}}>
                        {button}
                        {cancelButton}
                    </Footer>}
                </Container>
            )
        }
    }
}

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders,
        total: state.total
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderStatus)

const modalStyles = StyleSheet.create({
    modal: {
        borderRadius: 8,
        backgroundColor: colors.white,
        zIndex:1000
    },
    header: {
        borderRadius: 8,
        paddingTop: 8,
        backgroundColor: colors.base
    },
    header1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 12,
        backgroundColor: colors.base
    },
    rightHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    headerText: {
        color: colors.white,
        fontSize: 18,
        fontFamily: firstFont,
        fontWeight: 'bold',
        textAlign:'center'
    },
    iconClose: {
        color: colors.white,
        fontFamily: iconFont,
        fontSize: 20
    },
    icon: {
        color: colors.white,
        fontSize: 24,
        fontFamily: iconFont,
    },
    iconFab: {
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 8
    },
    messageContainer: {
        padding: 10,
        maxHeight: 300,
        backgroundColor: colors.gray95
    },
    version: {
        fontFamily: firstFont,
        fontWeight: 'bold',
        textAlign: 'right'
    },
    changeLog: {
        fontFamily: firstFont,
        color: colors.gray46,
        textAlign: 'right',
        marginRight: 8,
        paddingBottom: 4,
        // margin:14
    },
    url: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'right',
        color: colors.base,
    },
    bottomRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        // margin: 16
    },
    button: {
        height: 45,
        alignItems: `center`,
        justifyContent: 'center',
        borderRadius: 8,
        shadowRadius: 3,
        shadowOpacity: 0.7,
        padding: 16
    },
    row: {
        paddingRight: 4
    },
    forceUpdateText:{fontSize: 24, textAlign: 'center',margin:10,color:'red',borderBottomWidth:1,borderBottomColor:'#ddd'}
});