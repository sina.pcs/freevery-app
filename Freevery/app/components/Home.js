import React, { Component } from 'react';
import {
    StyleSheet,
    AsyncStorage,
} from 'react-native';

import CategoryPage from './categories/CategoryPage';

import * as userActions from '../redux/actions/userActions';
import * as navActions from '../redux/actions/navActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import configureStore from '../redux/store/configureStore';
import styles from '../styles';
import {NavigationActions} from 'react-navigation';

import { Container, Content, Button,Thumbnail,Card,CardItem, InputGroup,Input,Icon,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row,Item } from 'native-base';


import {STORAGE_KEY} from '../constants/AppConstants'
const store = configureStore();


class Home extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this._loadInitialState().done();
    }

    componentWillMount() {
        this.cid = this.props.navigation.state.params && this.props.navigation.state.params.cid;
        this.totalNumberOfOrders = 0;

        this.props.orders.map(
            (order, index) => {
                this.totalNumberOfOrders += order.quantity;
            }
        );
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.nav.orderId && nextProps.nav.orderId !== this.props.nav.orderId){
            this.props.navigation.navigate('OrderStatus',{id:nextProps.nav.orderId});
            this.props.actions.resetOrderId();
        }
    }

    _loadInitialState = async () => {
        try {
            var value = await AsyncStorage.getItem(STORAGE_KEY);
            if (value !== null){
                this.props.actions.addUser(JSON.parse(value));
            }
        } catch (error) {

        }
    };

    render() {
        let totalNumberOfOrders = 0;
        this.props.orders.map(
            (order, index) => {
                totalNumberOfOrders += order.quantity;
            }
        );
        return (
            <Container style={styles.container}>
                <Content>
                    <CategoryPage cid={this.cid}/>
                </Content>
            </Container>
        );
    }
}

const pageStyles = StyleSheet.create({
    button:{
        textAlign:'center'},
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
    rtl:{
        textAlign:'right'
    }
});

function mapStateToProps(state, ownProps) {
    return {
        nav: state.nav,
        users: state.users,
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        actions: bindActionCreators({...userActions,...navActions}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
