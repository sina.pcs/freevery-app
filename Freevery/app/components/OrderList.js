import React, {Component} from 'react';
import {connect} from 'react-redux';
import OneOrderInList from './OneOrderInList';
import {bindActionCreators} from 'redux';
import * as orderActions from '../redux/actions/orderActions';
import {View, ScrollView, ImageBackground} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Spinner,
    Grid,
    List,
    ListItem,
    Content,
    Container,
    Row,
    Col,
    Footer
} from 'native-base';
import Text from './Text'
import {colors, firstFont} from '../constants/AppConstants';
import styles from '../styles';
class OrdersList extends Component {
    constructor(props, context) {
        super(props);
        this.increase = this.increase.bind(this);
        this.decrease = this.decrease.bind(this);
        this.delete = this.delete.bind(this);
        this.navigate = this.navigate.bind(this);
    }

    increase(e, index, price) {
        e.preventDefault();
        this.props.actions.increaseOrder(index);
        this.props.actions.increaseTotal(parseFloat(price));
    }

    decrease(e, index, price) {
        e.preventDefault();
        if (this.props.orders[index].quantity > 1) {
            this.props.actions.decreaseOrder(index);
            this.props.actions.decreaseTotal(parseFloat(price));
        }
    }

    delete(index, price, quantity) {
        this.props.actions.deleteOrder(index);
        this.props.actions.decreaseTotal(parseFloat(price) * quantity);
    }

    navigate() {
        if (this.props.orders.length) {
            if (this.props.users.length)
                this.props.navigation.navigate('Checkout');
            else
                this.props.navigation.navigate('Profile');
        }
        else {
            alert('سبد خرید خالی است')
        }
    }

    render() {
        let rows = [];
        let totalNumberOfOrders = 0;

        function total() {
            return 0;
        }

        rows = this.props.orders.map(
            (order, index) => <OneOrderInList increase={(e) => this.increase(e, index, order.price)}
                                              decrease={(e) => this.decrease(e, index, order.price)}
                                              delete={() => this.delete(index, order.price, order.quantity)}
                                              order={order} key={index} editable={true}
                                              lastRow={this.props.orders.length - 1 === index}
            />
        );

        this.props.orders.map(
            (order, index) => {
                totalNumberOfOrders += order.quantity;
            }
        );

        return (
            <Container style={styles.container}>
                <ImageBackground source={require('../assets/bag.png')}
                                 style={{width: '100%', flex: 1, flexDirection: 'column'}}>
                    <ScrollView style={{padding: 10, flex: 1,backgroundColor:'rgba(255,255,255,0.9)'}}>
                        {rows}
                    </ScrollView>
                </ImageBackground>
                <View style={{backgroundColor: colors.second, height: 60}}>
                    <Row>
                        <Col size={1}>
                            <Button block success large onPress={this.navigate} style={{borderRadius: 0}}>
                                <Text style={{color: '#fff', fontSize: 16}}> تایید سفارش</Text>
                            </Button>
                        </Col>
                        <Col size={1} style={{justifyContent: 'center'}}>
                            <Text style={{
                                color: colors.base,
                                textAlign: 'center',
                                fontFamily: firstFont
                            }}>{ this.props.total } تومان</Text>
                        </Col>
                    </Row>
                </View>
            </Container>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders,
        total: state.total,
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList)
