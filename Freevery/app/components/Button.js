import React, { Component } from 'react';
import {
  View,
  Text,
    TouchableOpacity
} from 'react-native';
import {Icon} from 'native-base';

import styles from '../styles';
export default class Button extends React.PureComponent {
  render() {
    return(
    <TouchableOpacity onPress={() => this.props.onPress()}>
        <View style={[styles.button,this.props.style]}>
            <Text style={[styles.buttonText,this.props.textStyle]}>{this.props.title}</Text>
            <Icon name={this.props.icon} style={[styles.buttonIcon,this.props.iconStyle]}/>
        </View>
    </TouchableOpacity>
    )    
  }
}
