import React from 'react';
import CheckoutForm from './CheckoutForm';
import MobileVerificationPage from './MobileVerificationPage';
import auth from '../auth';
import {View} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import { Actions } from 'react-native-router-flux';

class CheckoutPage extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            pageState: 0,
        };
    }
    componentWillMount(){
        if(this.props.users.length)
        {
            this.setState({
                pageState: 1
            });
        }
    }
    render() {
        switch (this.state.pageState)
        {
            case 0:
                return <MobileVerificationPage changeState={()=>this.setState({pageState:1})}/>
            case 1:
                return <CheckoutForm changeState={(orderId) => { Actions.orderStatus({id:orderId}); }} />

        }
    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage)