import React from 'react';
import { View, StyleSheet, Image,TouchableOpacity  } from 'react-native';
import { Button,Spinner,Thumbnail,Icon,ListItem,List,Grid,Col,Row,Body,Navigator } from 'native-base';
import Lightbox from 'react-native-lightbox';

import Text from '../Text';
import styles from '../../styles';
const pageStyles = StyleSheet.create({
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
});
const ProductButton = (props) => {
    const imageName = props.product.image_name?props.product.image_name:props.product.code+'.jpg';
    let image = {uri:'https://freevery.com/images/products/thumbnail/'+imageName};
    return(
            <ListItem style={{paddingTop:0,
                paddingRight:0,
                paddingBottom:1,
                margin:5,
                marginLeft:10,
                marginRight:10,
                borderRadius: 4,
                borderWidth: 1,
                borderColor: '#ddd'

            }}>
                    <View style={[styles.Row,{padding:0,backgroundColor:'#fff'}]}>
                        <View style={{flex:2}}>
                            <View style={[styles.Row,{flex:3}]}>
                                <View style={{flex:3}}>
                                    <View style={{flex:1,padding:10}}>
                                        <Text style={{textAlign:'right',fontSize:12}}>{props.product.title}</Text>
                                    </View>
                                </View>
                            </View>
                            {/*<View style={[styles.Row,{flex:2}]}>*/}
                                {/*<View style={{flex:1,paddingRight:10}}>*/}
                                    {/*<Text style={{textAlign:'right'}} note>{props.product.description}</Text>*/}
                                {/*</View>*/}
                            {/*</View>*/}
                            <View style={[styles.Row,{flex:3,padding:10,borderTopWidth:1,borderTopColor:'#ddd'}]}>
                                <View style={{flex:1}}>
                                    <Text style={{textAlign:'center',width:'100%',fontSize:12}}>
                                        {props.product.price} تومان
                                    </Text>
                                </View>
                            </View>

                                <TouchableOpacity onPress={()=>props.addThis(props.product)} style={[styles.Row,{marginTop:5,flex:1,borderTopWidth:1,borderTopColor:'#ddd',padding:10}]}>
                                    <Text style={{textAlign:'center',color:'#34a853',flex:1,fontSize:12}}>
                                        اضافه به سبد خرید
                                    </Text>
                                    <Icon name="md-add-circle" style={{fontSize: 20,color: '#34a853'}}/>
                                </TouchableOpacity>
                        </View>
                        <View style={{flex:1,padding:10,borderLeftWidth:1,borderLeftColor:'#ddd'}}>
                            <Lightbox style={{ flex:1 }}>
                                <Image
                                    style={{ height:'100%' }} source={{uri: image.uri}} />
                            </Lightbox>
                        </View>

                    </View>
                </ListItem>
)};

export default ProductButton;