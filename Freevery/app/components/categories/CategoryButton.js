import React from 'react';
import { Container, Content, Button,Thumbnail,Card,CardItem, InputGroup,Input,Icon,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row } from 'native-base';
import {View,Text,InteractionManager,TouchableHighlight} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../../styles';

class CategoryButton extends React.Component {
    constructor(props, context) {
        super(props, context);
    }
    goToCategory = (cid) => {
        InteractionManager.runAfterInteractions(() => {
            Actions.home({cid:cid});
        });
    };
    render() {

        let category = this.props.category;
        return (
                <Col style={{marginRight:this.props.margin}}>
                    <Card>
                        <TouchableHighlight button style={{padding:0}} onPress={() => this.goToCategory(category.id)}>
                            <View style={{display:'flex',flexDirection:'column',alignItems:'center'}}>
                                <View style={{flex:1,padding:20}}>
                                    <Text style={{fontSize:20}}>{category.title}</Text>
                                </View>
                                <View style={{flex:1,backgroundColor:'#f5f5f5',padding:10,width:'100%'}}>
                                    <Text note style={{textAlign:'right'}}>شامل : </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </Card>
                </Col>
        );
    }
}

export default CategoryButton;