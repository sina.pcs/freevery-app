import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ListView,
    TouchableOpacity,
    TouchableHighlight,
    AsyncStorage,
    Platform,
    RefreshControl,
} from 'react-native';
import Toast from 'react-native-root-toast';
import ProductButton from './ProductButton';
import * as orderActions from '../../redux/actions/orderActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import configureStore from '../../redux/store/configureStore';
import styles from '../../styles';
import products from '../../data/products';
import axios from 'axios';

import { Container,Content,Spinner,Thumbnail,Icon,ListItem,List,Grid,Col,Row } from 'native-base';

class Products extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            dataSource: [],
            toast:false,
            pageState:0
        };
        this.addThis = this.addThis.bind(this);
    }
    addThis(product) {
        let orderIndex = this.props.orders.findIndex(order => order.pid == product.id);
        if (orderIndex != -1) {
            this.props.actions.increaseOrder(orderIndex);
        }
        else {
            let order = {pid: product.id, title: product.title, quantity: 1, price: product.price, categoryId:product.category_id};
            this.props.actions.addOrder(order);
        }

        this.props.actions.increaseTotal(parseInt(product.price));


            let toastText = product.title + ' به سبد خرید اضافه شد'
            Toast.show(toastText, {
                duration: Toast.durations.LONG,
                position: -60,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                onShow: () => {
                    this.setState({
                        toast: true
                    });
                },
                onShown: () => {
                    // calls on toast\`s appear animation end.
                },
                onHide: () => {
                    this.setState({
                        toast: false
                    })
                },
                onHidden: () => {
                    // calls on toast\`s hide animation end.
                }
            });

    }
    componentWillMount() {
        let products_url = 'https://freevery.com/api/web/product/get-by-catid?cat_id='+this.props.cid;
        axios.get(products_url).then((result)=>{
                const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                this.setState({
                    dataSource:ds.cloneWithRows(result.data.data),
                    pageState:1
                })
            })
            .catch((error)=>{debugger});

    }
    render() {
        switch (this.state.pageState){
            case 0:
                return(
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                )
            break;
            case 1:
                return (
                    <View>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={(data) => {
                                return <ProductButton product={data} addThis={(product)=>{ this.addThis(product)}} />
                                }
                            }

                        />
                    </View>

                );
        }
    }
}

const pageStyles = StyleSheet.create({
    button:{
        textAlign:'center'},
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
    rtl:{
        textAlign:'right'
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },
});

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);

