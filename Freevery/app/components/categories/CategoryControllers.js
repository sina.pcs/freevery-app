import React from 'react';
import MyInput from '../../common/input'

class CategoriesController extends React.Component{
    goBack = function (e) {
        window.history.back();
    };
    render() {
        return (
            <div>
                <MyInput className="search" name="search" type="text" label="جستجو محصول یا دسته مورد نظر" />

                <div id="products-navigation-bar">
                    <div className="col-md-2 col-xs-6">
                        <Link to="/" className="btn">خانه</Link>
                    </div>
                    <div className="col-md-2  col-xs-6">
                        <a className="btn" onClick={this.goBack}>بازگشت</a>
                    </div>
                    <div className="col-md-7 col-xs-12 currentDirectory">
                        شاخه فعلی : {this.props.thisCategoryTitle}
                    </div>
                </div>
            </div>
        )
    }
}

export default CategoriesController;