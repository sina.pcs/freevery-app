import React from 'react'
import {View,Image,ScrollView,StyleSheet} from 'react-native';
import Text from '../Text'
import {Button,Icon,Card} from 'native-base';
import Categories from './Categories';
import Products from './Products';
import categoriesData from '../../data/categories.json';
import { Actions } from 'react-native-router-flux';
import ProductButtonInHorizontalScroller from './ProductButtonInHorizontalScroller';
import axios from 'axios';
import * as orderActions from '../../redux/actions/orderActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Toast from 'react-native-root-toast';

class CategoryPage extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            featuredProducts : []
        }
        this.getCategoriesOrProducts = this.getCategoriesOrProducts.bind(this);
        this.addThis = this.addThis.bind(this);

    }
    componentWillMount () {
        this.thisCategory = 1;

        if(this.props.cid)
            this.thisCategory = this.props.cid;

        this.getCategoriesOrProducts();
    }

    findById(element) {
        return element.id == this.thisCategory;
    }

    openDrawer = () => {
        Actions.refresh({key: 'drawer', open: true });
    };

    getCategoriesOrProducts() {
        // let categories = categoriesData.filter(category => category.parent == this.thisCategory);
        // if (categories.length == 0) {
            let _this = this;
            if(this.thisCategory == 1){
                let products_url = 'https://freevery.com/api/web/product/get-featured';
                axios.get(products_url).then((result)=>{
                    _this.setState({
                        featuredProducts: result.data.data
                    });
                })
                    .catch((error)=>{});

            }
            else{
                this.rows = <Products cid={this.thisCategory}/>;
            }
        // }
        // else
        //     this.rows = <Categories cid={this.thisCategory} categories={categories}/>;

    }
    //thisCategoryTitle = categoriesData.find(this.findById).title;

    addThis(product) {
        let orderIndex = this.props.orders.findIndex(order => order.pid == product.id);
        if (orderIndex != -1) {
            this.props.actions.increaseOrder(orderIndex);
        }
        else {
            let order = {pid: product.id, title: product.title, quantity: 1, price: product.price, categoryId:product.category_id};
            this.props.actions.addOrder(order);
        }

        this.props.actions.increaseTotal(parseInt(product.price));


        let toastText = product.title + ' به سبد خرید اضافه شد'
        Toast.show(toastText, {
            duration: Toast.durations.LONG,
            position: -60,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {
                this.setState({
                    toast: true
                });
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                this.setState({
                    toast: false
                })
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });

    }

    render(){
        let featuredProducts = [];
        for(let i=0; i<this.state.featuredProducts.length; i++){
            featuredProducts.push(
                <ProductButtonInHorizontalScroller key={i} {...this.state.featuredProducts[i]} addThis={()=>{ this.addThis(this.state.featuredProducts[i])}} />
            )
        }
        return (
            <View>
                {this.thisCategory==1?
                    <View>

                        <View style={pageStyles.row}>
                            <View style={pageStyles.introContainer}>
                                <Image style={pageStyles.image} source={require('../../assets/payments.png')} />
                                <Text>پرداخت آنلاین</Text>
                                <Text>یا پرداخت در محل</Text>
                            </View>
                            <View style={pageStyles.introContainer}>
                                <Image style={pageStyles.image} source={require('../../assets/delivery.png')} />
                                <Text>ارسال رایگان سفارش</Text>
                                <Text>در کمتر از 1 ساعت</Text>
                            </View>
                        </View>
                        <View style={pageStyles.row}>
                            <View style={pageStyles.introContainer}>
                                <Image style={pageStyles.image} source={require('../../assets/tehran.png')} />
                                <Text>پوشش منطقه 1 تهران</Text>
                                <Text>و به زودی سایر مناطق</Text>
                            </View>
                            <View style={pageStyles.introContainer}>
                                <Image style={pageStyles.image} source={require('../../assets/clock.png')} />
                                <Text>8 صبح تا 10 شب</Text>
                                <Text>شماره تماس: 44148536</Text>
                            </View>
                        </View>

                        <Text style={{color:'#000',margin:10,padding:10}}>محصولات منتخب</Text>
                        <ScrollView horizontal={true}
                                    ref={ref => this.scrollView2 = ref}
                                    onContentSizeChange={(contentWidth, contentHeight)=>{
                                        this.scrollView2.scrollToEnd({animated: false});
                                    }}>
                            {featuredProducts}
                        </ScrollView>

                    </View>
                    :this.rows}

            </View>
        )
    }
}

const pageStyles = StyleSheet.create({
    badge:{
        color:'#fff',
        fontSize:10,
        textAlign:'center',
        margin:5,
        padding:10,
        backgroundColor:'#2c3e50',
        borderRadius:100,
        width:120
    },
    row:{
      flexDirection: 'row'
    },
    introContainer:{
        flex:1,
        alignItems:'center',
        margin:10
    },
    image:{
        flex:1
    }
});

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
