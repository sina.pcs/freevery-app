import React from 'react';
import { View, StyleSheet, Image,TouchableOpacity  } from 'react-native';
import Text from '../Text';
import { Button,Spinner,Thumbnail,Icon,ListItem,List,Grid,Col,Row,Body } from 'native-base';
import Lightbox from 'react-native-lightbox';

import styles from '../../styles';
const pageStyles = StyleSheet.create({
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
});

class ProductButtonInHorizontalScroller extends React.Component {
    render() {
        const imageName = this.props.image_name?this.props.image_name:this.props.code+'.jpg';
        let image = {uri: 'https://freevery.com/images/products/thumbnail/' + imageName};
        return(

            <View style={{backgroundColor: '#fff',margin:5, borderWidth: 1, borderColor: '#ddd',width:150,paddingTop:10}}>
                    <View style={[styles.Row]}>
                        <Lightbox style={{ flex:1,height:100 }}>
                            <Image
                                style={{ height:'100%' }}
                                resizeMode="contain"
                                source={{uri: image.uri}} />
                        </Lightbox>
                    </View>
                    <View style={[styles.Row, {flex: 1, padding: 10}]}>
                        <Text style={{textAlign: 'center',width:'100%', fontSize: 12}}>{this.props.title}</Text>
                    </View>
                    <View style={[styles.Row, { padding: 10,backgroundColor:'#efefef'}]}>
                        <View style={{flex: 1}}>
                            <Text style={{textAlign: 'center', width: '100%'}}>
                                {this.props.price} تومان
                            </Text>
                        </View>
                    </View>

                    <TouchableOpacity onPress={this.props.addThis} style={[styles.Row, {
                        padding: 10
                    }]}>
                        <Text style={{textAlign: 'center', color: '#34a853', flex: 1, fontSize: 10}}>
                            اضافه به سبد خرید
                        </Text>
                        <Icon name="md-add-circle" style={{fontSize: 20, color: '#34a853'}}/>
                    </TouchableOpacity>

            </View>
        )
    }
    ;
}

export default ProductButtonInHorizontalScroller;