import React from 'react';
import CategoryButton from './CategoryButton';
import { Container, Content, Button,Thumbnail,Card,CardItem, InputGroup,Input,Icon,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row } from 'native-base';
import {Text} from 'react-native';
import { Actions } from 'react-native-router-flux';
class Categories extends React.Component {
    constructor(props, context) {
        super(props, context);
    }
    componentWillMount () {
        this.rows = this.fetch();
    }
    fetch(){
        let thisCategory = this.props.cid;
        let categories = this.props.categories;
        let rows = [];
        for(let counter=0;counter<categories.length;counter=counter+2){
            rows.push(
                <Row style={{paddingLeft:10,paddingRight:10}} key={counter}>
                    <CategoryButton category={categories[counter]} margin={10}/>
                    <CategoryButton category={categories[counter+1]} margin={0}/>
                </Row>
            );
        }
        return rows;
    }
    render() {

        //return _.map(categories, (category,key) => <CategoryButton category={category} margin={0} key={category.id}/>);
        // return categories.map(category => <CategoryButton category={category} margin={0} key={category.id}/>);


        return (
            <Grid>
                {this.rows}
            </Grid>
        );
    }
}

export default Categories;