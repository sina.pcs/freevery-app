import React from 'react';
import { StyleSheet, Text, View,AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col
} from 'native-base';
import styles from '../styles';

class WaitPage extends React.Component {
    constructor(props, context) {
        super(props, context);
    }
    render() {
        return (
            <Container style={styles.container}>
                <Content style={{padding:20}}>
                    <View style={{alignItems:'center'}}>
                        <Spinner/>
                        <Text style={{textAlign:'center'}}>
                            سفارش شما در انتظار تایید است. لطفا منتظر بمانید.
                        </Text>
                    </View>
                </Content>
            </Container>

        )
    }
}


export default WaitPage;