import React from 'react';
import {
    SwitchIOS,
    View,
    Text,
    Modal,
    AsyncStorage
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container
} from 'native-base';
import styles from '../styles';


class MobileVerification extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            validationError: '',
            pageState: 0, //show form state
            mobile: '',
            pass1:'',
            pass2:'',
            password: '',
            smsCode: '',
            minToActivateButton:'01',
            secToActivateButton:'00',
            reSendButton:false,
        };
        this.mobileSubmit = this.mobileSubmit.bind(this);
        this.checkCode = this.checkCode.bind(this);
        this.signup = this.signup.bind(this);
        this.initializeClock = this.initializeClock.bind(this);
        this.sendSms = this.sendSms.bind(this);
    }
    componentWillUnmount(){
        clearInterval(this.timeinterval);
    }
    getTimeRemaining(deadline) {
        var t = Date.parse(deadline) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        return {
            'total': t,
            'minutes': minutes,
            'seconds': seconds
        };
    }
    static timeinterval = null;
    initializeClock() {
        clearInterval(this.timeinterval);
        let deadline = new Date(Date.parse(new Date()) +  1 * 60 * 1000);
        let t = this;
        this.timeinterval = setInterval((t) => {
            var t = this.getTimeRemaining(deadline);

            this.setState({minToActivateButton:('0' + t.minutes).slice(-2)});
            this.setState({secToActivateButton:('0' + t.seconds).slice(-2)});


            if (t.total <= 0) {
                this.setState({reSendButton:true});
                clearInterval(this.timeinterval);
                this.setState({minToActivateButton:'0'});
                this.setState({secToActivateButton:'0'});
            }
        }, 1000);
    }

    MobileValidation = function (mobile) {

        let mobileRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;

        let re = /[A-Za-z\d]{8,}/;

        if (!mobileRegex.test(mobile)) {
            this.setState({
                validationError: 'شماره وارد شده معتبر نیست',
                pageState: 0,
                mobile: mobile
            });
            return false;
        }
        this.setState({
            validationError:''
        });
        return true;
    };

    PasswordValidation = function (pass1, pass2) {

        let re = /[A-Za-z\d]{8,}/;

        if (!re.test(pass1)) {
            this.setState({
                validationError: 'کلمه عبور حداقل باید 8 کاراکتر داشته باشد',
                pageState: 2
            });
            return false;
        }

        if (pass1 != pass2) {
            this.setState({
                validationError: 'کلمه عبور و تکرار کلمه عبور یکسان نیستند',
                pageState: 2
            });
            return false
        }

        this.setState({
            validationError:''
        });
        return true;
    };

    mobileSubmit() {
        let mobile = this.state.mobile;

        if (this.MobileValidation(mobile)) {
            this.sendSms();
        }

    }
    sendSms(){
        this.setState({
            pageState:4
        })
        //TODO Request to send sms


        // axios.get('http://newwebsite.ir/esmyab/login.php?phone=' + this.state.mobile)
        //     .then((response) => {
        //         if (response.status == 200) {
        //             this.setState({signUpState: 1});
        //             this.setState({reSendButton: false});
        //             this.initializeClock();
        //         }
        //     });

        //on Response
        this.setState({pageState: 1,reSendButton: false});
        this.initializeClock();
    }
    checkCode() {
        let smsCode = this.state.smsCode;
        //TODO Request to check code
        this.setState({
            pageState: 2
        })
        //Else
        // this.setState({
        //     signUpState: 1,
        //     validationError: 'کد وارد شده اشتباه است'
        // })

    }
    signup() {
        let password = this.state.pass1;
        let passwordConfirm = this.state.pass2;

        if (this.PasswordValidation(password, passwordConfirm)) {
            //TODO Request to signup
            this.setState({
                password: password,
                pageState: 3
            })
        }

    }
    render() {
        switch (this.state.pageState) {
            case 0 :
                return (
                    <Container style={this.props.noPadding?'':styles.container}>
                        <Content>
                            <ValidationError error={this.state.validationError}/>
                            <InputGroup iconRight borderType='underline' style={{margin: 20}}>
                                <Input onChangeText={(mobile) => this.setState({mobile})}
                                       value={this.state.mobile}
                                       keyboardType="numeric"
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="شماره تلفن همراه"/>
                                <Icon name="md-person"/>
                            </InputGroup>
                            <Text style={{textAlign: 'center', padding:20}}>
                                پیامکی برای شما ارسال خواهد شد که شامل یک کد امنیتی است. پس از تایید کد امنیتی شما وارد برنامه خواهید شد.
                            </Text>
                            <Button block info large style={{margin: 20}} onPress={this.mobileSubmit}>
                                <Text>تایید</Text>
                            </Button>
                        </Content>
                    </Container>
                );
            case 1 :
                return (

                    <Container style={styles.container}>
                        <Content>
                            <ValidationError error={this.state.validationError}/>
                            <Text style={{textAlign: 'center', padding:20}}>
                                کد امنیتی به شماره {this.state.mobile} پیامک شد.
                            </Text>
                            <InputGroup iconRight borderType='underline' style={{margin: 20}}>
                                <Input onChangeText={(smsCode) => this.setState({smsCode})}
                                       value={this.state.smsCode}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="کد ارسال شده از طریق پیامک را وارد کنید"/>
                                <Icon name="md-person"/>
                            </InputGroup>
                            <Button block info large style={{margin: 20}} onPress={this.checkCode}>
                                <Text>تایید</Text>
                            </Button>
                            <Text style={{textAlign: 'center', padding:20}}>
                                دکمه ارسال مجدد بعد از 5 دقیقه برای شما فعال خواهد شد. در صورت عدم دریافت پیامک پس از 5 دقیقه این دکمه را لمس کنید.
                            </Text>
                            <Text style={{textAlign: 'center', padding:20,fontSize:30}}>
                                {this.state.minToActivateButton}:{this.state.secToActivateButton}
                            </Text>
                            <Button block large disabled={!this.state.reSendButton} style={{margin: 20}} onPress={this.sendSms}>
                                <Text>ارسال مجدد پیامک</Text>
                            </Button>
                        </Content>
                    </Container>
                );
            case 2 :
                return (
                    <Container style={styles.container}>
                        <Content>

                            <ValidationError error={this.state.validationError}/>
                            <ValidationError error={this.state.validationError}/>
                            <InputGroup iconRight borderType='underline' style={{margin: 20}}>
                                <Input onChangeText={(pass1) => this.setState({pass1})}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       secureTextEntry={true}
                                       placeholder="کلمه عبور"/>
                                <Icon name="md-key"/>
                            </InputGroup>
                            <InputGroup iconRight borderType='underline' style={{margin: 20}}>
                                <Input onChangeText={(pass2) => this.setState({pass2})}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       secureTextEntry={true}
                                       placeholder="تکرار کلمه عبور"/>
                                <Icon name="md-key"/>
                            </InputGroup>
                            <Button block info large style={{margin: 20}} onPress={this.signup}>
                                <Text>تایید</Text>
                            </Button>
                        </Content>
                    </Container>
                );
            case 3 :
                return (
                <Container style={styles.container}>
                    <Content>

                        <Text style={{textAlign: 'center', padding:20}}>
                            ثبت نام با موفقیت انجام شد.
                        </Text>
                        <Button block info large style={{margin: 20}} onPress={this.props.changeState}>
                            <Text>ادامه خرید</Text>
                        </Button>
                    </Content>
                </Container>
            );

            case 4:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                )
        }

    }
}


class ValidationError extends React.Component {
    render() {
        if (this.props.error != '') {
            return (
                <Text style={{padding: 20, color: 'red', textAlign:'center'}}>
                    {this.props.error}
                </Text>
            )
        }
        else
            return <Text/>
    }
}


export default MobileVerification;