import React, { Component } from 'react';
import {View} from 'react-native';
import { Container, Content, Button,Thumbnail,Card,CardItem, InputGroup,Input,Icon,Body,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row } from 'native-base';
import styles from '../styles';
import Text from './Text'
class OneOrderInList extends Component{
    constructor(props) {
        super(props);
        this.state = {willBeDeleted: false};
        this.delete = this.delete.bind(this);
    }
    delete(e){
        e.preventDefault();
        if(!this.state.willBeDeleted) {
            this.setState({willBeDeleted: true});
            setTimeout(() => {
                this.setState({willBeDeleted: false});
                this.props.delete();
            }, 500);
        }
    }
    render(){
        let order = this.props.order;
        return(
            <View style={{marginBottom:this.props.lastRow?30:0}}>
                <Card style={{backgroundColor:this.state.willBeDeleted?'#e74c3c':'#fff'}}>
                    <View style={{padding:10}}>
                    <View style={styles.Row}>
                        <View style={{flex:1}}>
                            <Text style={{textAlign:'right',fontFamily:'IRANSansMobile_Bold',color:'#000'}}>{order.title}</Text>
                        </View>
                    </View>

                    {this.props.editable &&
                    <View style={{flexDirection:'row',marginTop:5}}>

                        <View style={{flexDirection:'column'}}>
                            <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                <Button transparent onPress={this.props.increase}
                                        style={{ height: 30,backgroundColor:'#eee'}}>
                                    <Icon name="ios-add"/>
                                </Button>
                                <Text style={{lineHeight: 28,width:35,textAlign:'center'}}>{order.quantity}</Text>
                                <Button transparent onPress={this.props.decrease}
                                        style={{height: 30,backgroundColor:'#eee'}}>
                                    <Icon name="ios-remove"/>
                                </Button>
                            </View>
                        </View>
                        <View style={{flex: 1,flexDirection:'row',justifyContent:'flex-end',alignItems:'flex-end'}}>

                            <Text style={{textAlign: 'right',fontSize:12,color:'#888',paddingRight:10}}>قیمت واحد: {order.price} تومان</Text>
                            <Button transparent onPress={this.delete} style={{height:30,backgroundColor:'#eee'}}>
                                <Icon name="md-trash" style={{color: 'red'}}/>
                            </Button>
                        </View>

                    </View>
                    }
                    {!this.props.editable &&
                    <View style={[styles.Row, {paddingTop: 10}]}>
                        <Text style={{textAlign: 'right', lineHeight: 30}}>تعداد: {order.quantity}</Text>

                        <View style={{flex: 1}}>
                            <Text style={{textAlign: 'right', lineHeight: 30}}>{order.price} ریال</Text>
                        </View>

                    </View>
                    }
                    </View>
                </Card>
            </View>
        )
    }
}

export default OneOrderInList;
