import React from 'react';
import { StyleSheet, View,AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    ListItem,
    List
} from 'native-base';
import styles from '../styles';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import axios from 'axios';
import Text from './Text';
import auth from '../auth';
let jalaali = require('jalaali-js');
import {colors} from '../constants/AppConstants';

class OrdersHistoryPage extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            pageState:0,
            orders:[]
        };
    }
    getToken = async () => {
        try {
            let token = await AsyncStorage.getItem('@freevery:token');
            return token;
        } catch (error) {
            return false;
        }
    };
    componentWillMount(){
        let user = this.props.users[0];
        let _this = this;
        this.getToken().done(function (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token ;
            axios.get('https://freevery.com/api/web/orders/get-order-by-customer-id-and-state?customer_id='+user.id)
                .then(function (response) {
                    if(response.data.code="1")
                    {
                        _this.setState({
                            pageState:1,
                            orders:response.data.data
                        })
                    }
                })
                .catch(function (error) {
                })
        });
    }
    render() {
        if(this.state.pageState == 0)
        {
            return(
                <Container style={styles.container}>
                    <Content>
                        <View style={{alignItems:'center'}}>
                            <Spinner />
                        </View>
                    </Content>
                </Container>
            )
        }
        else{
            let orders = this.state.orders.map((order,i) => {
                let t = order.insert_datetime.split(/[- :]/);
                let j = jalaali.toJalaali(parseInt(t[0]), parseInt(t[1]), parseInt(t[2]));
                let bg = (i%2)?'white':'transparent';
                return <ListItem
                    style={{marginLeft:0,
                        backgroundColor:bg,
                        paddingLeft:20,
                        paddingRight:20,
                        borderColor: '#eeeeee',
                        borderBottomWidth:0.5,
                        justifyContent:'flex-end'}}
                     button
                     onPress={() => this.props.navigation.navigate('OrderStatus',{id:order.id})}
                     key={'order'+order.id}>
                    <Icon name="ios-arrow-back" style={{color:colors.base,marginRight:30,fontSize:20}} />
                    <Text>{j.jy + '/' + j.jm + '/' + j.jd}</Text>
                    <Text style={{flex:1}}>
                        سفارش شماره {order.id}
                    </Text>
                    <Icon name="ios-cart" style={{color:colors.base,marginLeft:10,fontSize:20}} />
                </ListItem>
            });
            return (
                <Container style={styles.container}>
                    <Content>
                        <List>
                            {orders}
                        </List>
                    </Content>
                </Container>

            )

        }
    }
}
function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}

export default connect(mapStateToProps)(OrdersHistoryPage)