import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Modal,
    ScrollView,
    AsyncStorage
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container,
    Item
} from 'native-base';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import Text from './Text';
import {NavigationActions} from 'react-navigation';
import styles from '../styles';

import {STORAGE_KEY,colors} from '../constants/AppConstants'

class Signup extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            validationError: '',
            mobile:'',
            pass1:'',
            pass2:'',
            smsCode:'',
            signUpState:0,
            minToActivateButton:'01',
            secToActivateButton:'00',
            reSendButton:false,
            username:'',
            phoneValid:false
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.sendSms = this.sendSms.bind(this);
        this.checkCode = this.checkCode.bind(this);
        this.initializeClock = this.initializeClock.bind(this);
    }

    getTimeRemaining(deadline) {
        var t = Date.parse(deadline) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        return {
            'total': t,
            'minutes': minutes,
            'seconds': seconds
        };
    }
    static timeinterval = null;
    initializeClock() {
        clearInterval(this.timeinterval);
        let deadline = new Date(Date.parse(new Date()) +  5 * 60 * 1000);
        let t = this;
        this.timeinterval = setInterval((t) => {
            var t = this.getTimeRemaining(deadline);

            this.setState({minToActivateButton:('0' + t.minutes).slice(-2)});
            this.setState({secToActivateButton:('0' + t.seconds).slice(-2)});


            if (t.total <= 0) {
                this.setState({reSendButton:true});
                clearInterval(this.timeinterval);
                this.setState({minToActivateButton:'0'});
                this.setState({secToActivateButton:'0'});
            }
        }, 1000);
    }

    onSubmit() {
        let mobile = this.state.mobile;
        let password = this.state.pass1;
        let passwordConfirm = this.state.pass2;

        if (this.Validation(mobile, password, passwordConfirm)) {
            this.setState({
                mobile: mobile,
                password: password
            });

            this.sendSms();
        }

    }

    sendSms(){
        this.setState({
            signUpState:3
        });

        axios.post('https://freevery.com/api/web/sms/send-sms', {
            headers: {
                'Content-Type': 'application/json'
            },
            mobile: this.state.mobile,
            device_type:'mobile'
        })
            .then((response) => {
                if (response.data.code == '1') {
                    this.setState({signUpState: 1,reSendButton: false});
                    this.initializeClock();
                }
                else{
                    this.setState({
                        signUpState: 0,
                        validationError: response.data.message
                    })
                }
            })
            .catch(function (error) {
                this.setState({
                    signUpState: 0,
                    validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                })
            });
    }

    Validation = function (mobile, pass1, pass2) {

        let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let mobileRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;

        let re = /[A-Za-z\d]{8,}/;

        if (!mobileRegex.test(mobile)) {
            this.setState({
                validationError: 'شماره وارد شده معتبر نیست',
                signUpState: 0,
                mobile: mobile
            });
            return false;
        }

        if (!re.test(pass1)) {
            this.setState({
                validationError: 'کلمه عبور حداقل باید 8 کاراکتر داشته باشد',
                signUpState: 0,
                mobile: mobile
            });
            return false;
        }

        if (pass1 != pass2) {
            this.setState({
                validationError: 'کلمه عبور و تکرار کلمه عبور یکسان نیستند',
                signUpState: 0,
                mobile: mobile,
            });
            return false
        }

        this.setState({
            validationError:''
        });
        return true;
    };

    checkCode() {
        this.setState({
            signUpState: 3
        });

        let _this = this;
        axios.post('https://freevery.com/api/web/sms/validate-code', {
            headers: {
                'Content-Type': 'application/json'
            },
            mobile: this.state.mobile,
            device_type: 'mobile',
            code: this.state.smsCode
        })
            .then((response) => {
                if (response.data.code == '1') {
                    _this.signup();
                }
                else{
                    _this.setState({
                        signUpState: 1,
                        validationError: 'کد وارد شده اشتباه است'
                    })
                }
            })
            .catch(function (error) {
                _this.setState({
                    signUpState: 1,
                    validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                })
            });

    }

    signup(){
        let _this = this;
        axios.post('https://freevery.com/api/web/user/sign-up', {
            headers: {
                'Content-Type': 'application/json'
            },
            username: this.state.mobile,
            password: this.state.password
        })
            .then((response) => {
                if (response.data.code == '1') {
                    _this.storeToken(response.data.token).done(function () {
                        _this.storeUser(JSON.stringify(response.data.data)).done(function () {
                            _this.addUserToStore();
                        });
                    });
                }
                else{
                    _this.setState({
                        signUpState: 1,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
                }
            })
            .catch(function (error) {
                _this.setState({
                    signUpState: 1,
                    validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                })
            });
    }
    storeUser = async (user) => {
        try {
            //await AsyncStorage.multiSet([[STORAGE_KEY_USERNAME, this.state.username], [STORAGE_KEY_mobile, this.state.mobile]]);
            await AsyncStorage.setItem(STORAGE_KEY,user);
        } catch (error) {
            // Error saving data
        }

    };
    storeToken = async (token) => {
        try {
            await AsyncStorage.setItem(STORAGE_KEY,token);
        } catch (error) {
            // Error saving data
        }

    };
    addUserToStore = async () => {
        let _this = this;
        try {
            let value = await AsyncStorage.getItem(STORAGE_KEY);
            debugger;
            if (value !== null){
                value = JSON.parse(value);
                _this.props.actions.addUser(value);
                const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'UserArea'})
                    ]
                });
                _this.props.navigation.dispatch(resetAction)
            }
        } catch (error) {
        }
    };
    close(){
        if(this.props && this.props.from && this.props.from == 'checkout')
            this.props.navigation.navigate('Checkout');
        else
            this.props.navigation.goBack();
    };

    componentWillUnmount(){
        clearInterval(this.timeinterval);
    }
    render() {
        switch(this.state.signUpState){
            case 0:
                return (
                    <ScrollView style={styles.container}>
                        <View style={{padding:50,justifyContent:'center',alignItems:'center'}}>
                            <ValidationError error={this.state.validationError}/>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(mobile) => this.setState({mobile})}
                                       value={this.state.mobile}
                                       keyboardType="numeric"
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="شماره همراه خود را وارد کنید"/>
                                <Icon name="md-person"/>
                            </Item>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(pass1) => this.setState({pass1})}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       secureTextEntry={true}
                                       placeholder="کلمه عبور"/>
                                <Icon name="md-lock"/>
                            </Item>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(pass2) => this.setState({pass2})}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       secureTextEntry={true}
                                       placeholder="تکرار کلمه عبور"/>
                                <Icon name="md-lock"/>
                            </Item>
                            <Text style={{textAlign: 'center', padding:20}}>
                                پیامکی برای شما ارسال خواهد شد که شامل یک کد امنیتی است. پس از تایید کد امنیتی شما وارد برنامه خواهید شد.
                            </Text>
                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.onSubmit}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>تایید</Text>
                            </Button>
                        </View>
                    </ScrollView>
                );
            break;
            case 1:
                return (
                    <ScrollView style={styles.container}>
                        <View style={{padding:50,justifyContent:'center',alignItems:'center'}}>
                            <ValidationError error={this.state.validationError}/>
                            <Text style={{textAlign: 'center', padding:20}}>
                                کد امنیتی به شماره {this.state.mobile} پیامک شد.
                            </Text>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(smsCode) => this.setState({smsCode})}
                                       value={this.state.smsCode}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="کد ارسال شده از طریق پیامک را وارد کنید"/>
                                <Icon name="md-key"/>
                            </Item>
                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.checkCode}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>تایید</Text>
                            </Button>
                            <Text style={{textAlign: 'center', padding:20}}>
                                دکمه ارسال مجدد بعد از 5 دقیقه برای شما فعال خواهد شد. در صورت عدم دریافت پیامک پس از 5 دقیقه این دکمه را لمس کنید.
                            </Text>
                            {!this.state.reSendButton &&
                                <Text style={{textAlign: 'center', padding:20,fontSize:30}}>
                                    {this.state.minToActivateButton}:{this.state.secToActivateButton}
                                </Text>
                            }
                            {this.state.reSendButton &&
                                <Button block disabled={!this.state.reSendButton} style={[styles.button,{backgroundColor:colors.base}]} onPress={this.sendSms}>
                                    <Text style={[styles.buttonText,{color:colors.white}]}>ارسال مجدد پیامک</Text>
                                </Button>
                            }

                        </View>
                    </ScrollView>
                );
                break;
            case 2:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <Text style={{textAlign: 'center', padding:20}}>
                                ثبت نام با موفقیت انجام شد.
                            </Text>
                            <Button block info large style={{margin: 20}} onPress={this.close}>
                              <Text style={{color:'#fff',fontSize:20}}>ادامه</Text>
                            </Button>

                        </Content>
                    </Container>
                );
                break;
            case 3:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                );
                break;
            default:
                return(
                    <View>

                    </View>
                )
        }

    }
}
class ValidationError extends Component {
    render() {
        if (this.props.error != '') {
            return (
                    <View style={styles.errorBox}>
                        <Text style={styles.errorText}>
                            {this.props.error}
                        </Text>
                        <Icon name="ios-alert" style={styles.errorIcon} />
                    </View>
            )
        }
        else
            return <Text/>
    }
}

function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup)