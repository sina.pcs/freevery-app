import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ListView
} from 'react-native';

import Text from './Text';

import Toast from 'react-native-root-toast';
import ProductButton from './categories/ProductButton';
import styles from '../styles';
import {colors} from '../constants/AppConstants'
import axios from 'axios';
import * as orderActions from '../redux/actions/orderActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import { Spinner,Container, Content, Button,Thumbnail,Card,CardItem, InputGroup,Input,Icon,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row,Item } from 'native-base';

let timeout = null;

class Search extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            searchVal : '',
            result:'',
            searched: 0
        }
    }
    addThis(product) {
        let orderIndex = this.props.orders.findIndex(order => order.pid == product.id);
        if (orderIndex != -1) {
            this.props.actions.increaseOrder(orderIndex);
        }
        else {
            let order = {pid: product.id, title: product.title, quantity: 1, price: product.price, categoryId:product.category_id};
            this.props.actions.addOrder(order);
        }

        this.props.actions.increaseTotal(parseInt(product.price));


        let toastText = product.title + ' به سبد خرید اضافه شد'
        Toast.show(toastText, {
            duration: Toast.durations.LONG,
            position: -60,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {
                this.setState({
                    toast: true
                });
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                this.setState({
                    toast: false
                })
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });

    }
    search(value){
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            if(value.length > 2) {
                this.setState({
                    searched : 1
                });
                let _this = this;
                axios.post('https://freevery.com/api/web/product/advanced-search',
                    {
                        "query": {
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "title": value
                                        }
                                    },


                                    {
                                        "prefix": { "title" :  { "value" : value, "boost" : 2.0 } }
                                    }
                                ]
                            }
                        },
                        "from" : 0, "size" : 1000
                    }

                )
                    .then(function (result) {
                        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                        _this.setState({
                            result: ds.cloneWithRows(JSON.parse(result.data).hits.hits),
                            searched: 2
                        })

                    })
            }
            else{
                this.setState({
                    result: null,
                    searched:0
                })
            }
        }, 500);
    }
    render() {
        let results = <Text/>;
        switch(this.state.searched){
            case 0:
                results = <Text/>;
                break;

            case 1:
                results = <View>
                        <Text style={{textAlign:'center',marginTop:50,fontSize:20,marginBottom:20}}>در حال جستجو</Text>
                        <Spinner />
                    </View>;
                break;

            case 2:
                results = <ListView
                    dataSource={this.state.result}
                    renderRow={(data) => {
                            product = data._source;
                            return <ProductButton product={product} addThis={(product)=>{ this.addThis(product)}} />
                        }
                    }

                />;
                break;
        }
        return (
            <Container style={styles.container}>
                <Header searchBar rounded style={{backgroundColor:colors.white,paddingTop:0}}>
                    <Button transparent>
                        <Icon name="ios-search"  />
                    </Button>
                    <Item>
                        <Input placeholder="جستجو" style={{backgroundColor:colors.white,textAlign:'right'}}
                               onChangeText = {(val) => this.search(val)}
                        />
                        <Icon name="ios-search"  />
                    </Item>
                </Header>
                <Content>
                    {results}
                </Content>
            </Container>
        );
    }
}

const pageStyles = StyleSheet.create({
    button:{
        textAlign:'center'},
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
    rtl:{
        textAlign:'right'
    }
});

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);