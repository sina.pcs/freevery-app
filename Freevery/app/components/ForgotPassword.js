import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Modal,
    ScrollView,
    AsyncStorage,
    TouchableOpacity,
    Animated
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container,
    Item
} from 'native-base';
import axios from 'axios';
import Text from './Text'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import { Actions } from 'react-native-router-flux';
import auth from '../auth';
import {NavigationActions} from 'react-navigation';
import Animation from 'lottie-react-native';


import styles from '../styles';
import {STORAGE_KEY,colors} from '../constants/AppConstants'

class ForgotPassword extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            mobile:'',
            password:'',
            pageState:0,
            error: false,
            animatedStartValue:new Animated.Value(0),
            doneAnimation:new Animated.Value(0)
        };

        this.onSubmit=this.onSubmit.bind(this);
        this.cycleAnimation = this.cycleAnimation.bind(this);
    }

    componentDidMount() {
        this.cycleAnimation()
    }
    cycleAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedStartValue, {
                    toValue: 1,
                    duration: 3000,
                    delay: 1000
                }),
                Animated.timing(this.state.animatedStartValue, {
                    toValue: 0,
                    duration: 3000
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }
    onSubmit() {
        let mobile = this.state.mobile;
        if(mobile != '') {
            this.setState({pageState:1})
            let objectToSend = {
                username: this.state.mobile,
                device_type:'mobile'
            }
            let _this = this;
            axios.post('https://freevery.com/api/web/user/forget-password', objectToSend)
                .then(function (response) {
                    if (response.data.code == '1') {
                        _this.setState({pageState: 2},()=>{
                            Animated.timing(this.state.doneAnimation, {
                                toValue: 1,
                                duration: 3000,
                            }).start();
                        })
                    }
                    else {
                        _this.setState({pageState: 0, error: response.data.message});

                    }
                })
                .catch(function (error) {
                    _this.setState({pageState: 0, error: 'مشکل ارتباط با سرور'});
                });
        }
    }
    render() {
        switch(this.state.pageState){
            case 0:
                return (
                    <ScrollView style={styles.container}>
                        <View style={{paddingHorizontal:50,paddingVertical:20,justifyContent:'center',alignItems:'center'}}>
                            <Animation
                                style={{
                                    width: 150,
                                    height: 150,
                                }}
                                progress={this.state.animatedStartValue}
                                source={require('../assets/animations/padlock_tick.json')}
                            />
                            {this.state.error && (
                                <View style={styles.errorBox}>
                                    <Text style={styles.errorText}>
                                        {this.state.error}
                                    </Text>
                                    <Icon name="ios-alert" style={styles.errorIcon} />
                                </View>
                            )}
                            <Text style={{textAlign:'center',paddingBottom:20}}>
                                شماره خود را وارد کنید تا کلمه عبور موقتی از طریق پیامک برای شما ارسال شود
                            </Text>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(mobile) => this.setState({mobile})}
                                       value={this.state.mobile}
                                       keyboardType="numeric"
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="شماره تلفن همراه"/>
                                <Icon name="ios-call" style={styles.inputIcon}/>
                            </Item>

                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.onSubmit}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>تایید</Text>
                            </Button>
                        </View>
                    </ScrollView>
                );
                break;
            case 1:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                );
                break;
            case 2:
                return (
                    <Container style={styles.container}>
                        <View style={{padding:50,justifyContent:'center',alignItems:'center'}}>
                            <Animation
                                style={{
                                    width: 200,
                                    height: 200,
                                }}
                                progress={this.state.doneAnimation}
                                source={require('../assets/animations/code_invite_success.json')}
                            />
                            <Text style={{textAlign: 'center', paddingVertical:20,fontSize:20}}>
                                رمز عبور موقت از طریق پیامک برای شما ارسال شد
                            </Text>
                            <Button block  style={[styles.button,{backgroundColor:colors.base}]} onPress={()=>this.props.navigation.goBack()}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>بازگشت</Text>
                            </Button>
                        </View>
                    </Container>
                );
                break;
            default:
                return(
                    <View>

                    </View>
                )
        }

    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)