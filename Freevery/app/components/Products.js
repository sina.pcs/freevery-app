import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    AsyncStorage,
    Platform
} from 'react-native';

import GridView from 'react-native-grid-view';
//import * as userActions from '../redux/actions/userActions';
import {bindActionCreators} from 'redux';
import {Provider,connect} from 'react-redux';
import configureStore from '../redux/store/configureStore';
import styles from '../styles';
import {loadState,saveState} from '../redux/store/localStorage';

import { Container, Content, Button,Thumbnail, InputGroup,Input,Icon,Header,Footer,FooterTab,Badge,ListItem,List,Grid,Col,Row } from 'native-base';

class Products extends Component {
    constructor(props, context) {
        super(props, context);
    }
    render() {
        return (
            <Container style={styles.container}>

                <Content>
                    <List>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                        <ListItem button style={styles.listItem}>
                            <Grid>
                                <Col style={{  }} size={3}>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text>کالباس 60% گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <View style={{flex:1}}>
                                            <Text note>نیکان گوشت</Text>
                                        </View>
                                    </Row>
                                    <Row size={1}>
                                        <Col size={1}>
                                            <Text style={{textAlign:'left'}}>
                                                200000 ریال
                                            </Text>
                                        </Col>
                                        <Col size={1}>
                                            <Text style={{textAlign:'right'}}>
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                                <Icon style={pageStyles.ribbon} name="md-star" />
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{  }} size={1}>
                                    <Thumbnail square style={{}} size={80} source={require('./../assets/20004.jpg')} />
                                </Col>
                            </Grid>
                        </ListItem>
                    </List>
                </Content>
                <Footer style={{backgroundColor:'#2c3e50'}} >
                    <FooterTab>
                        <Button>
                            <Badge>2</Badge>
                            سبد خرید
                            <Icon name='ios-basket' />
                        </Button>
                        <Button>
                            بخش کاربری
                            <Icon name='ios-contact-outline' />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const pageStyles = StyleSheet.create({
    button:{
        textAlign:'center'},
    ribbon:{
        fontSize:20,color:'#D4AF37'
    },
    rtl:{
        textAlign:'right'
    }
});

function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);

