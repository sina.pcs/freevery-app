import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Modal,
    ScrollView,
    AsyncStorage,
    TouchableOpacity,
    Animated
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container,
    Item
} from 'native-base';
import axios from 'axios';
import Text from './Text'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import { Actions } from 'react-native-router-flux';
import auth from '../auth';
import {NavigationActions} from 'react-navigation';
import Animation from 'lottie-react-native';


import styles from '../styles';
import {STORAGE_KEY,colors} from '../constants/AppConstants'

class ChangePassword extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            validationError: '',
            pageState: 0, //show form state
            password: '',
            prevPassword: '',
            passwordConfirm: '',
            animatedStartValue:new Animated.Value(0),
            doneAnimation:new Animated.Value(0)
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.changePassword = this.changePassword.bind(this);
    }

    Validation = function (pass1, pass2) {

        let re = /[A-Za-z\d]{8,}/;

        if (!re.test(pass1)) {
            this.setState({
                validationError: 'کلمه عبور حداقل باید 8 کاراکتر داشته باشد',
                signUpState: 0,
            });
            return false;
        }

        if (pass1 != pass2) {
            this.setState({
                validationError: 'کلمه عبور و تکرار کلمه عبور یکسان نیستند',
                signUpState: 0,
            });
            return false
        }

        return true;
    };

    onSubmit() {

        let prevPassword = this.state.prevPassword;
        let password = this.state.password;
        let passwordConfirm = this.state.passwordConfirm;

        if (this.Validation(password, passwordConfirm)) {
            this.setState({
                prevPassword: prevPassword,
                password: password,
                pageState: 1,
                validationError: ''

            },this.changePassword)
        }
    }

    changePassword = async () =>{
        this.setState({pageState:1});
        let _this = this;
        let token = await AsyncStorage.getItem('@freevery:token');
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token ;

        let objectToSend = {
            'pre-password': this.state.prevPassword,
            'password': this.state.password,
        }

        axios.put('https://freevery.com/api/web/user/change-password',
            objectToSend
        )
            .then((response) => {

                if (response.data.code == '1') {
                    this.storeToken(response.data.token);
                    _this.setState({pageState: 2},()=>{
                        Animated.timing(this.state.doneAnimation, {
                            toValue: 1,
                            duration: 3000,
                        }).start();
                    });
                }
                else{
                    _this.setState({
                        pageState: 0,
                        validationError: response.data.message
                    })
                }
            })
            .catch(function (error) {
                _this.setState({
                    pageState: 0,
                    validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                })
            });
    }
    storeToken = async (token) => {
        try {
            await AsyncStorage.setItem('@freevery:token',token);
        } catch (error) {
            // Error saving data
        }

    };
    componentDidMount() {
        this.cycleAnimation()
    }
    cycleAnimation() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.animatedStartValue, {
                    toValue: 1,
                    duration: 3000,
                    delay: 1000
                }),
                Animated.timing(this.state.animatedStartValue, {
                    toValue: 0,
                    duration: 3000
                })
            ]),
            {
                iterations: 10
            }
        ).start()
    }
    render() {
        switch(this.state.pageState){
            case 0:
                return (
                    <ScrollView style={styles.container}>
                        <View style={{paddingHorizontal:50,paddingVertical:20,justifyContent:'center',alignItems:'center'}}>

                            <Animation
                                style={{
                                    width: 150,
                                    height: 150,
                                }}
                                progress={this.state.animatedStartValue}
                                source={require('../assets/animations/padlock_tick.json')}
                            />
                            {this.state.validationError !== '' && (
                                <View style={styles.errorBox}>
                                    <Text style={styles.errorText}>
                                        {this.state.validationError}
                                    </Text>
                                    <Icon name="ios-alert" style={styles.errorIcon} />
                                </View>
                            )}
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(prevPassword) => this.setState({prevPassword})}
                                       value={this.state.prevPassword}
                                       secureTextEntry={true}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="کلمه عبور فعلی"/>
                                <Icon name="ios-lock" style={styles.inputIcon}/>
                            </Item>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(password) => this.setState({password})}
                                       value={this.state.password}
                                       secureTextEntry={true}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="کلمه عبور جدید"/>
                                <Icon name="ios-lock" style={styles.inputIcon}/>
                            </Item>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(passwordConfirm) => this.setState({passwordConfirm})}
                                       value={this.state.passwordConfirm}
                                       secureTextEntry={true}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="تکرار کلمه عبور جدید"/>
                                <Icon name="ios-lock" style={styles.inputIcon}/>
                            </Item>

                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.onSubmit}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>تایید</Text>
                            </Button>
                        </View>
                    </ScrollView>
                );
                break;
            case 1:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                );
                break;
            case 2:
                return (
                    <Container style={styles.container}>
                        <View style={{padding:50,justifyContent:'center',alignItems:'center'}}>
                            <Animation
                                style={{
                                    width: 200,
                                    height: 200,
                                }}
                                progress={this.state.doneAnimation}
                                source={require('../assets/animations/code_invite_success.json')}
                            />
                            <Text style={{textAlign: 'center', paddingVertical:20,fontSize:20}}>
کلمه عبور با موفقیت تغییر کرد
                            </Text>
                            <Button block  style={[styles.button,{backgroundColor:colors.base}]} onPress={()=>this.props.navigation.goBack()}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>بازگشت</Text>
                            </Button>
                        </View>
                    </Container>
                );
                break;
            default:
                return(
                    <View>

                    </View>
                )
        }

    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)