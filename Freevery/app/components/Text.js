
import React from 'react';
import styles from '../styles';
import {
    Text
} from 'react-native';

export default class Pext extends React.PureComponent {
    render() {
        return (

            <Text style={[styles.text,this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}

