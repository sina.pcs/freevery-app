import React from 'react';
import { StyleSheet, Text, View,AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col
} from 'native-base';
const Item = Picker.Item;
import styles from '../styles';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';

import {STORAGE_KEY} from '../constants/AppConstants'

class CheckoutForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        user = props.users[0];
        this.state = {
            phoneNumber:user.phoneNumber,
            name:user.name?user.name:'',
            area:user.area?user.area:undefined,
            address:user.address?user.address:'',
            email:user.email?user.email:'',
            selectedType: undefined,
            selectedItem: undefined,
            checkoutState:0
        };
        this.saveOrder = this.saveOrder.bind(this);
    }
    onValueChange (value) {
        this.setState({
            area : value
        });
    }
    saveOrder = async () =>{
        let username = this.props.users[0].username;
        let user = {
            username:username,
            phoneNumber:this.state.phoneNumber,
            name:this.state.name,
            area:this.state.area,
            address:this.state.address,
            email:this.state.email
        };
        this.setState({checkoutState:1});
        try {
            //await AsyncStorage.multiSet([[STORAGE_KEY_USERNAME, this.state.username], [STORAGE_KEY_PHONENUMBER, this.state.phoneNumber]]);
            await AsyncStorage.setItem(STORAGE_KEY,JSON.stringify(user));
        } catch (error) {
            // Error saving data
        }
        this.props.actions.updateUser(user);


        //request to server
        //on response
        Actions.wait();

    };
    render() {
        return (
            <Container style={styles.container}>
                <Content style={{padding:20}}>

                    <InputGroup iconRight borderType='underline' style={{marginBottom: 20}}>
                        <Input onChangeText={(name) => this.setState({name})} style={{textAlign: 'right', paddingRight: 10}}
                               placeholder="نام و نام خانوادگی" value={this.state.name}/>
                        <Icon name="md-person"/>
                    </InputGroup>
                    <InputGroup iconRight borderType='underline' style={{marginBottom: 20}}>
                        <Input onChangeText={(phoneNumber) => this.setState({phoneNumber})} style={{textAlign: 'right', paddingRight: 10}}
                               placeholder="تلفن همراه" value={this.state.phoneNumber}/>
                        <Icon name="md-call"/>
                    </InputGroup>
                    <InputGroup iconRight borderType='underline' style={{marginBottom: 20}}>
                        <Input onChangeText={(email) => this.setState({email})} style={{textAlign: 'right', paddingRight: 10}}
                               placeholder="ایمیل" value={this.state.email}/>
                        <Icon name="md-mail"/>
                    </InputGroup>
                    <Grid>
                        <Row style={{borderBottomWidth:0.5,borderColor:'#d9d5dc',height:40}}>
                            <Col size={6}>
                                <View>
                                    <Picker
                                        iosHeader="نوع درخواست"
                                        mode="dropdown"
                                        prompt="نوع درخواست"
                                        selectedValue={this.state.area}
                                        onValueChange={this.onValueChange.bind(this)}>
                                        <Item style={{fontSize:12}} label="منطقه" value="brand"  />
                                        <Item label="ونک" value="ونک" />
                                        <Item label="جردن" value="جردن" />
                                        <Item label="میرداماد" value="میرداماد" />
                                    </Picker>
                                </View>
                            </Col>
                            <Col size={4}>
                                <Text style={{textAlign: 'right',paddingTop:10,paddingRight:10,fontSize:15,color:'#000'}}>
                                    انتخاب منطقه
                                </Text>
                            </Col>
                            <Col size={1}>
                                <Icon name="md-home" style={{paddingTop:5}}/>
                            </Col>
                        </Row>
                    </Grid>
                    <Textarea
                        value={this.state.address}
                        placeholder="آدرس"
                        onChangeText={(address) => this.setState({address})}
                        style={{textAlign: 'right',height:120, paddingRight: 10, borderBottomWidth:0.5,borderColor:'#d9d5dc',fontSize:15}} />

                </Content>
                <Footer style={{backgroundColor:'#2c3e50'}} >
                    <Button block successs large onPress={this.saveOrder}>
                        تایید
                    </Button>
                </Footer>
            </Container>

        )
    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutForm)