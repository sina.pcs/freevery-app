import React from 'react';
import {
    SwitchIOS,
    View,
    Modal,
    TouchableHighlight,
    AsyncStorage
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container
} from 'native-base';
import styles from '../styles';
import Text from './Text';
import Login from './Login';
import Signup from './Signup';
import MobileVerification from './MobileVerification';

class MobileVerificationPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            panel:0
        };
    }
    render() {
        return (
            <Container style={styles.container}>
                <Content>
                    <Text style={{padding:10,textAlign:'center'}}>
                        در صورتی که قبلا در ثبت نام کرده اید لطفا از فرم اول استفاده کرده و وارد شوید.
                        اما در صورتی که قبلا ثبت نام نکرده اید شماره همراه خود را در فرم دوم وارد کنید.
                    </Text>
                    <TouchableHighlight onPress={()=>{this.state.panel!=1?this.setState({panel:1}):this.setState({panel:0})}}>
                        <View>
                            <Text style={styles.panelTitle}>ورود</Text>
                        </View>
                    </TouchableHighlight>
                    {this.state.panel == 1 &&
                        <Login noPadding={true}/>}

                    <TouchableHighlight onPress={()=>{this.state.panel!=2?this.setState({panel:2}):this.setState({panel:0})}}>
                        <View>
                            <Text style={styles.panelTitle}>ثبت نام</Text>
                        </View>
                    </TouchableHighlight>
                    {this.state.panel == 2 &&
                        /*<MobileVerification noPadding={true} changeState={()=>this.props.changeState()} />*/
                        <Signup noPadding={true}/>
                    }
                </Content>
            </Container>

        )
    }
}

export default MobileVerificationPage;