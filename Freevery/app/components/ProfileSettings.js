import React from 'react';
import { StyleSheet, Text, View,AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    InputGroup,
    Input,
    Icon,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col,
    Button,
    Item
} from 'native-base';
import styles from '../styles';
import {STORAGE_KEY,colors,firstFont} from '../constants/AppConstants';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import axios from 'axios';
import Animation from 'lottie-react-native';


class ProfileSettings extends React.Component {
    constructor(props, context) {
        super(props, context);
        user = props.users[0];
        this.state = {
            first_name:user.first_name?user.first_name:'',
            last_name:user.last_name?user.last_name:'',
            address:user.address?user.address:[],
            email:user.email?user.email:'',
            mobile:user.username,
            id:user.id,
            selectedType: undefined,
            selectedItem: undefined,
            pageState:0
        };
        this.saveUser = this.saveUser.bind(this);
    }
    componentDidMount() {
        this.animation.play();
    }
    onValueChange (value) {
        this.setState({
            area : value
        });
    }
    saveUser = async () =>{
        this.setState({pageState:1});
        let _this = this;
        let token = await AsyncStorage.getItem('@freevery:token');
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token ;

        let objectToSend = {
            "first_name": this.state.first_name,
            "last_name": this.state.last_name,
            "email": this.state.email
        };
        let user = this.props.users[0];

        axios.put('https://freevery.com/api/web/user/update',
            objectToSend
        )
            .then((response) => {
                if (response.data.code == '1') {
                    user.first_name = response.data.data.first_name;
                    user.last_name = response.data.data.last_name;
                    user.email = response.data.data.email;
                    _this.updateUser(user).done(function () {
                        _this.props.actions.updateUser(user);
                        _this.setState({pageState:0});
                        _this.props.navigation.goBack();
                    });
                }
                else{
                    _this.setState({
                        pageState: 0,
                        validationError: response.data.message
                    })
                }
            })
            .catch(function (error) {

                _this.setState({
                    pageState: 0,
                    validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                })
            });


    };
    updateUser = async(user)=>{
        try {
            //await AsyncStorage.multiSet([[STORAGE_KEY_USERNAME, this.state.username], [STORAGE_KEY_PHONENUMBER, this.state.phoneNumber]]);
            await AsyncStorage.setItem(STORAGE_KEY,JSON.stringify(user));
        } catch (error) {
            // Error saving data
        }
    }
    render() {
        switch (this.state.pageState){
            case 0:
                return (
                    <Container style={styles.container}>
                        <View style={{padding:20,justifyContent:'center',alignItems:'center',flex:1}}>
                            <Animation
                                ref={animation => { this.animation = animation; }}
                                style={{
                                    width: 100,
                                    height: 100,
                                }}
                                source={require('../assets/animations/outline_user.json')}
                            />
                            {this.state.validationError && (
                                <View style={styles.errorBox}>
                                    <Text style={styles.errorText}>
                                        {this.state.validationError}
                                    </Text>
                                    <Icon name="ios-alert" style={styles.errorIcon} />
                                </View>
                            )}
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(first_name) => this.setState({first_name})} style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="نام" value={this.state.first_name}/>
                                <Icon name="md-person"/>
                            </Item>

                            <Item rounded style={styles.input}>
                                <Input onChangeText={(last_name) => this.setState({last_name})} style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="نام خانوادگی" value={this.state.last_name}/>
                                <Icon name="md-person"/>
                            </Item>

                            <Item rounded style={styles.input}>
                                <Input style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="تلفن همراه" value={this.state.mobile}/>
                                <Icon name="md-call"/>
                            </Item>

                            <Item rounded style={styles.input}>
                                <Input onChangeText={(email) => this.setState({email})} style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="ایمیل" value={this.state.email}/>
                                <Icon name="md-mail"/>
                            </Item>
                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.saveUser}>
                                <Text style={{fontSize:20,color:'#fff',fontFamily:firstFont}}>
                                    تایید
                                </Text>
                            </Button>

                        </View>
                    </Container>

                )
                break;
            case 1:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                );
                break;
        }

    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettings)