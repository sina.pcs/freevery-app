import React, {Component} from 'react';
import OneOrderInList from './OneOrderInList';
import {
    SwitchIOS,
    View,
    Text,
    Modal,
    AsyncStorage
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col,
    List,
    ListItem,
    Radio
} from 'native-base';
import styles from '../styles';
import axios from 'axios';

class Order extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            order: {},
            pageState:0
        };
    }
    getToken = async () => {
        try {
            let token = await AsyncStorage.getItem('@freevery:token');
            return token;
        } catch (error) {
            return false;
        }
    };
    componentDidMount(){
        let order = this.props.order;
        let _this = this;
        this.getToken().done(function (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token ;
            axios.get('https://freevery.com/api/web/orders/get-by-orderid?order_id='+order.id)
                .then(function (response) {
                    if(response.data.code="1")
                    {
                        _this.setState({
                            pageState:1,
                            order:response.data.data[0]
                        })
                    }
                })
                .catch(function (error) {
                    debugger;
                })
        });
    }
    render() {
        this.rows = [];
        this.orderStatus = '';
        let totalNumberOfOrders = 0;

        function total() {
            return 0;
        }

        if(this.state.order.orderDetail){
            for(let i = 0; i < this.state.order.orderDetail.length ; i++){
                let order = this.state.order.orderDetail[i].product[0];
                order.price = this.state.order.orderDetail[i].price;
                order.quantity = this.state.order.orderDetail[i].count;
                this.rows.push(<OneOrderInList editable={false}
                                               order={order} key={'o'+i}/>)
            }


            switch (this.state.order.state){
                case '0':
                    this.orderStatus = 'در انتظار تایید سوپرمارکت';
                    break;
                case '1':
                    this.orderStatus = 'در انتظار تایید شما';
                    this.button =
                        <Button block successs large style={{width:'100%'}}>
                            <Text style={{fontSize:20,color:'#fff'}}>
                                تایید سفارش
                            </Text>
                        </Button>;
                    break;
                case '2':
                    this.orderStatus = 'در انتظار تایید شما';
                    this.button =
                        <Button block successs large style={{width:'100%'}}>
                            <Text style={{fontSize:20,color:'#fff'}}>
                                تایید سفارش
                            </Text>
                        </Button>;
                    break;
                case '3':
                    this.orderStatus = 'پرداخت ناموفق'
                    break;
                case '4':
                    this.orderStatus = 'تایید کاربر'
                    break;
                case '5':
                    this.orderStatus = 'در راه'
                    break;
                case '6':
                    this.orderStatus = 'تحویل داده شد'
                    break;
                case '7':
                    this.orderStatus = 'بسته شد'
                    break;

            }

        }



        if(this.state.pageState == 0)
        {
            return(
                <Container style={styles.container}>
                    <Content>
                        <View style={{alignItems:'center'}}>
                            <Spinner />
                        </View>
                    </Content>
                </Container>
            )
        }
        else {
            return (
                <Container style={styles.container}>
                    <Content>
                        <Text style={{textAlign:'center',fontSize:16,backgroundColor:'#ddd',padding:10,flex:1}}>
                            {this.orderStatus}
                        </Text>
                        {this.rows}
                        {this.button}
                    </Content>
                </Container>
            )
        }
    }
}

export default Order
