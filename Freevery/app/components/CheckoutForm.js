import React from 'react';
import auth from '../auth';
import {
    SwitchIOS,
    View,
    ScrollView,
    StyleSheet,
    AsyncStorage
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Footer,
    Spinner,
    Grid,
    Content,
    Container,
    Row,
    Col,
    Card,
    List,
    ListItem,
    Radio,
    CheckBox,
    Item
} from 'native-base';
const PickerItem = Picker.Item;
import Modal from './Modal';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import * as orderActions from '../redux/actions/orderActions';
import styles from '../styles';
import axios from 'axios';
import Text from './Text';
import {STORAGE_KEY, colors, firstFont, iconFont} from '../constants/AppConstants';

const areas = [
    {id: '0' , name:'انتخاب منطقه'},
    {id: '1', name: 'ونک'},
    {id: '2', name: 'میرداماد'},
    {id: '3', name: 'جردن'},
];

class CheckoutForm extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            first_name: '',
            mobile: '',
            email:'',
            last_name: '',
            address: [],
            pageState: 0,
            addressAddedBefore: false,
            selectedAddress:0,
            payment_method: '',
            requiredFieldsError : null,
            showPopup: false
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.appendAddress=this.appendAddress.bind(this);
        this.editAddress=this.editAddress.bind(this);
        this.areaChanged=this.areaChanged.bind(this);
        this.onAddressChanged = this.onAddressChanged.bind(this);
    }
    componentDidMount(){
        if(this.props.total < 15000 && this.props.orders.findIndex(product=>product.pid == 1) === -1)
            this.setState({showPopup:true});

        if(this.props.users.length){
            let user = this.props.users[0];
            this.setState({
                first_name:user.first_name?user.first_name:'',
                mobile:user.mobile?user.mobile:'',
                email:user.email?user.email:'',
                last_name:user.last_name?user.last_name:'',
                address:user.address?user.address:[],
            });
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    onAddressChanged(e){
        this.setState({
            selectedAddress: parseInt(e.currentTarget.value)
        });
    }
    appendAddress() {
        let newInput = {"address":"","area_id":'0',"areaName":''};
        this.setState({
            address: this.state.address.concat([newInput]),
            addressAddedBefore: true,
            selectedAddress: this.state.address.length
        });
    }
    areaChanged(i,area_id){
        let address = this.state.address;
        address[i].area_id = area_id;
        address[i].areaName = areas.find((a) => {return a.id == area_id}).name;
        this.setState({ address: address });
    }
    editAddress(i,addressValue) {
        let address = this.state.address;
        address[i].address = addressValue;
        this.setState({ address: address });
    }

    handleSubmit = async()=>{
        if(this.state.first_name == '' || this.state.last_name == '' || this.state.email=='' || this.state.payment_method=='')
        {
            this.setState({
                requiredFieldsError : 'پر کردن تمامی موارد الزامی است'
            });
            this.refs._scrollView.scrollTo({x:0,y:0,animated:true});
            return;
        }
        if(this.state.address.length == 0)
        {
            this.setState({
                requiredFieldsError : 'با کلیک بر روی دکمه اضافه کردن آدرس، آدرس خود را اضافه کنید.'
            });
            this.refs._scrollView.scrollTo({x:0,y:0,animated:true});
            return;
        }
        let address = this.state.address[this.state.selectedAddress];
        if(address.address == '' || address.area_id == 0 )
        {
            this.setState({
                requiredFieldsError : 'منطقه و آدرس را به درستی وارد کنید'
            });
            this.refs._scrollView.scrollTo({x:0,y:0,animated:true});
            return;
        }


        this.setState({
            pageState: 1
        });

        let _this = this;
        let token = await AsyncStorage.getItem('@freevery:token');
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token ;

        let user = this.props.users[0];
        let orders = this.props.orders.map((order) => {
            return {
                "product_id":order.pid,
                "price":order.price,
                "count":order.quantity,
                "product_title":order.title,
                "product_category_id":order.categoryId
            }
        });
        let objectToSend = {
            "customer_fullname": this.state.first_name + ' ' + this.state.last_name,
            "customer_mobile": user.username,
            "customer_address":address.address,
            "area_id":address.area_id,
            "area":  areas.find((a) => {
                return a.id == address.area_id
            }).name,
            "payment_method":this.state.payment_method,
            "addresses":this.state.address,
            "orders":orders
        }
        axios.post('https://freevery.com/api/web/orders/create', objectToSend)
            .then(function (response) {
                if (response.data.code == '1') {
                    user.address = _this.state.address;
                    _this.updateUser(user).done(function () {
                        _this.props.actions.updateUser(user);
                        _this.props.actions.clearOrder();
                        _this.props.navigation.goBack();
                        _this.props.navigation.navigate('OrderStatus',{id:response.data.data.id});
                    });
                }
                else{
                    _this.setState({pageState: 0,requiredFieldsError:response.data.message});

                }
            })
            .catch(function (error) {
                _this.setState({pageState: 0});
            })
    }
    updateUser = async(user)=>{
        try {
            //await AsyncStorage.multiSet([[STORAGE_KEY_USERNAME, this.state.username], [STORAGE_KEY_PHONENUMBER, this.state.phoneNumber]]);
            await AsyncStorage.setItem(STORAGE_KEY,JSON.stringify(user));
        } catch (error) {
            // Error saving data
        }
    };
    addDeliveryProduct = () => {
        this.props.actions.addOrder({
            categoryId:"1",
            pid:"1",
            price:"2000",
            quantity:1,
            title:"هزینه ارسال"
        });
        this.props.actions.increaseTotal(2000);
        this.setState({showPopup:false})
    };
    render() {
        let modalContent = <View>
            <View style={modalStyles.header}>
                <View style={modalStyles.header1}>
                    <View style={modalStyles.rightHeader}>
                        <Text style={modalStyles.headerText}>هزینه ارسال</Text>
                    </View>
                </View>
            </View>
            <ScrollView style={modalStyles.messageContainer}>
                <Text style={{textAlign:'center'}}>
                    با توجه به اینکه مقدار سفارش شما از 15000 تومان کمتر است 2000 تومان هزینه ارسال به سبد خرید
                    شما اضافه خواهد شد.
                </Text>

                <Button onPress={this.addDeliveryProduct} danger block style={{marginTop:10}}>
                    <Text style={[styles.buttonText, {color: 'white'}]}>تایید و ادامه سفارش</Text></Button>

                <Button onPress={()=>this.props.navigation.goBack()} warning block style={{marginTop:10}}>
                    <Text style={[styles.buttonText, {color: 'white'}]}>افزایش محصول به سبد خرید</Text>
                </Button>
            </ScrollView>
        </View>;
        switch (this.state.pageState) {
            case 0 :
                return (
                    <View>
                        <Modal
                            open={this.state.showPopup}
                            modalStyle={modalStyles.modal}
                            ref="modal"
                            closeOnTouchOutside={false}
                        >
                            {modalContent}
                        </Modal>
                    <ScrollView style={{padding: 10}} ref='_scrollView'>
                        <View style={{alignItems:'center'}}>
                            {this.state.requiredFieldsError &&
                            <View style={styles.errorBox}>
                                <Text style={styles.errorText}>
                                    {this.state.requiredFieldsError}
                                </Text>
                                <Icon name="ios-alert" style={styles.errorIcon} />
                            </View>
                            }

                            <Card style={{padding: 20,width:'100%'}}>
                                <Text style={{fontSize: 16, color: '#999', marginBottom: 20, textAlign: 'center',width:'100%'}}>
مشخصات گیرنده سفارش
                                </Text>
                                <View>
                                <Item rounded style={{...styles.input,width:'100%',flex:1}}>
                                    <Input onChangeText={(first_name) => this.setState({first_name})}
                                           style={{textAlign: 'right', paddingRight: 10}}
                                           placeholder="نام"
                                           value={this.state.first_name}/>
                                    <Icon name="ios-person"/>
                                </Item>

                                <Item rounded style={{...styles.input,width:'100%',flex:1}}>
                                    <Input onChangeText={(last_name) => this.setState({last_name})}
                                           style={{textAlign: 'right', paddingRight: 10}}
                                           placeholder="نام خانوادگی"
                                           value={this.state.last_name}/>
                                    <Icon name="ios-person"/>
                                </Item>

                                <Item rounded style={{...styles.input,width:'100%',flex:1}}>
                                    <Input onChangeText={(email) => this.setState({email})}
                                           style={{textAlign: 'right', paddingRight: 10}}
                                           placeholder="ایمیل"
                                           value={this.state.email}/>
                                    <Icon name="ios-mail"/>
                                </Item>
                                </View>
                            </Card>
                            <Card style={{ padding: 10,width:'100%'}}>
                                <Text style={{fontSize: 16, color: '#999', marginBottom: 20, textAlign: 'center'}}>
                                    آدرس گیرنده سفارش
                                </Text>
                                {this.state.address.map((input, i) => {
                                    return (
                                        <Card key={'L' + i}>
                                            <View style={[styles.Row, {
                                                paddingHorizontal:10,
                                                backgroundColor:colors.second
                                            }]}>
                                                <View style={{flex: 1}}>
                                                    <Picker
                                                        mode="dialog"
                                                        selectedValue={input.area_id}
                                                        onValueChange={(value) => {
                                                            this.areaChanged(i, value);
                                                        }}
                                                        style={{padding:0}}
                                                        itemStyle={{color:'#ddd'}}
                                                        itemTextStyle={{textAlign: 'right', flex: 1, fontFamily:firstFont}}
                                                        textStyle={{textAlign: 'right', flex: 1, fontFamily:firstFont}}
                                                    >
                                                        {areas.map((area, index) => {
                                                            return (
                                                                <PickerItem value={ area.id }
                                                                            color='#00B386'
                                                                      label={ area.name }
                                                                             style={{backgroundColor:'#000'}}
                                                                      key={ 'area' + i + index }>
                                                                </PickerItem>
                                                            );
                                                        })
                                                        }
                                                    </Picker>
                                                </View>
                                                <View style={{paddingTop: 10, width: 40}}>
                                                    <CheckBox checked={this.state.selectedAddress === i}
                                                              style={{marginLeft: 10}}
                                                              onPress={() => {
                                                                  this.setState({selectedAddress: i})
                                                              }}
                                                    />
                                                </View>
                                            </View>
                                            <View style={{borderBottomWidth: 0.5, borderColor: '#d9d5dc'}}>
                                            <Textarea
                                                value={input.address}
                                                onChangeText={(address) => {
                                                    this.editAddress(i, address)
                                                }}
                                                key={'address' + i}
                                                placeholder="آدرس"
                                                style={{
                                                    textAlign: 'right',
                                                    marginTop: 0,
                                                    paddingRight: 10,
                                                    fontSize: 12,
                                                    backgroundColor: '#fff',
                                                    fontFamily:firstFont
                                                }}/>

                                            </View>
                                        </Card>

                                    )
                                })}
                                {
                                    !this.state.addressAddedBefore &&
                                    <Button rounded block bordered onPress={ () => this.appendAddress() }
                                            style={{marginTop: 10}}>
                                        <Text>اضافه کردن آدرس</Text>
                                    </Button>
                                }
                            </Card>
                            <Card style={{ padding: 10,width:'100%'}}>
                                <Text style={{fontSize: 16, textAlign: 'center', color: '#999', marginBottom: 20}}>
                                    نوع پرداخت
                                </Text>
                                <List>
                                    <ListItem
                                        onPress={() => {
                                            this.setState({payment_method: "1"})
                                        }}>
                                        <Text style={{flex: 1, textAlign: 'right'}}>پرداخت آنلاین</Text>
                                        <CheckBox style={{marginLeft: 10}}
                                                  checked={this.state.payment_method === "1"}
                                        />
                                    </ListItem>
                                    <ListItem
                                        onPress={() => {
                                            this.setState({payment_method: "2"})
                                        }}>
                                        <Text style={{flex: 1, textAlign: 'right'}}>پرداخت نقدی درب منزل</Text>
                                        <CheckBox style={{marginLeft: 10}}
                                                  checked={this.state.payment_method === "2"}
                                        />
                                    </ListItem>
                                    <ListItem onPress={() => {
                                        this.setState({payment_method: "3"})
                                    }}>
                                        <Text style={{flex: 1, textAlign: 'right'}}>پرداخت از طریق دستگاه POS درب
                                            منزل</Text>
                                        <CheckBox style={{marginLeft: 10}}
                                                  checked={this.state.payment_method === "3"}
                                        />
                                    </ListItem>
                                </List>

                                <Text style={{
                                    marginTop: 20,
                                    backgroundColor: '#f2dede',
                                    color: '#a94442',
                                    padding: 10,
                                    textAlign: 'right'
                                }}>
                                    دقت فرمایید که تمامی تامین کنندگان ما دارای دستگاه POS نمی باشند و در صورت انتخاب
                                    روش پرداخت از طریق دستگاه POS ممکن است سفارش شما دیرتر از سایر روش ها تایید گردد.
                                </Text>

                            </Card>
                            <Button block
                                    style={[styles.button,{backgroundColor:colors.base,marginHorizontal:20,marginBottom:40}]}
                                    onPress={this.handleSubmit}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>ثبت سفارش</Text>
                            </Button>
                        </View>
                    </ScrollView>
                    </View>
                )
                break;
            case 1:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems: 'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                )
                break;

        }
    }
}



function mapStateToProps(state, ownProps) {
    return {
        users: state.users,
        orders: state.orders,
        total: state.total
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...userActions, ...orderActions }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutForm)

const modalStyles = StyleSheet.create({
    modal: {
        borderRadius: 8,
        backgroundColor: colors.white,
        zIndex:1000
    },
    header: {
        borderRadius: 8,
        paddingTop: 8,
        backgroundColor: colors.base
    },
    header1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 12,
        backgroundColor: colors.base
    },
    rightHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    headerText: {
        color: colors.white,
        fontSize: 18,
        fontFamily: firstFont,
        fontWeight: 'bold',
        textAlign:'center'
    },
    iconClose: {
        color: colors.white,
        fontFamily: iconFont,
        fontSize: 20
    },
    icon: {
        color: colors.white,
        fontSize: 24,
        fontFamily: iconFont,
    },
    iconFab: {
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 8
    },
    messageContainer: {
        padding: 10,
        maxHeight: 300,
        backgroundColor: colors.gray95
    },
    version: {
        fontFamily: firstFont,
        fontWeight: 'bold',
        textAlign: 'right'
    },
    changeLog: {
        fontFamily: firstFont,
        color: colors.gray46,
        textAlign: 'right',
        marginRight: 8,
        paddingBottom: 4,
        // margin:14
    },
    url: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'right',
        color: colors.base,
    },
    bottomRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        // margin: 16
    },
    button: {
        height: 45,
        alignItems: `center`,
        justifyContent: 'center',
        borderRadius: 8,
        shadowRadius: 3,
        shadowOpacity: 0.7,
        padding: 16
    },
    row: {
        paddingRight: 4
    },
    forceUpdateText:{fontSize: 24, textAlign: 'center',margin:10,color:'red',borderBottomWidth:1,borderBottomColor:'#ddd'}
});