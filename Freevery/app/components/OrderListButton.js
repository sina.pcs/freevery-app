import React, {Component} from 'react';
import {connect} from 'react-redux';
import OneOrderInList from './OneOrderInList';
import {bindActionCreators} from 'redux';
import * as orderActions from '../redux/actions/orderActions';
import {View,Text} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Picker,
    Textarea,
    Spinner,
    Content,
    Container,
    Row,
    Col
} from 'native-base';
import styles from '../styles';


class OrderListButton extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            orders: []
        };
    }
    componentWillMount(){
        this.totalNumberOfOrders = 0;

        this.props.orders.map(
            (order, index) => {
                this.totalNumberOfOrders += order.quantity;
            }
        );

    }
    render() {


        return (
                <Badge>{this.totalNumberOfOrders}</Badge>
        )
    }
}
function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderListButton)
