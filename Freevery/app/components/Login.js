import React, {Component} from 'react';
import {
    SwitchIOS,
    View,
    Text,
    Modal,
    ScrollView,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';
import {
    Button,
    InputGroup,
    Input,
    Icon,
    Spinner,
    Content,
    Container,
    Item
} from 'native-base';
import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../redux/actions/userActions';
import { Actions } from 'react-native-router-flux';
import auth from '../auth';
import {NavigationActions} from 'react-navigation';

import styles from '../styles';
import {STORAGE_KEY,colors} from '../constants/AppConstants'

class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            mobile:'',
            password:'',
            loginState:0,
            error: false,
        };

        this.onSubmit=this.onSubmit.bind(this);
        this.addUserToStore = this.addUserToStore.bind(this);
    }

    componentWillMount(){

    }
    onSubmit() {
        let mobile = this.state.mobile;
        let pass = this.state.password;

        if(mobile != '' && pass != '') {
            this.setState({loginState: 1});

            auth.login(mobile, pass, (loggedIn) => {
                if (!loggedIn)
                    return this.setState({error: true, loginState: 0});

                this.addUserToStore().done();

            })
        }
    }
    addUserToStore = async () => {
        let _this = this;
        try {
            let value = await AsyncStorage.getItem(STORAGE_KEY);
            if (value !== null){
                value = JSON.parse(value);
                _this.props.actions.addUser(value);
                const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'UserArea'})
                    ]
                });
                _this.props.navigation.dispatch(resetAction)

            }
        } catch (error) {
        }
    };
    close(){
        if(this.props && this.props.from && this.props.from == 'checkout')
            Actions.checkout();
        else
            Actions.pop();
    };
    render() {
        switch(this.state.loginState){
            case 0:
                return (
                    <ScrollView style={styles.container}>
                        <View style={{padding:50,justifyContent:'center',alignItems:'center'}}>
                            {this.state.error && (
                                <View style={styles.errorBox}>
                                    <Text style={styles.errorText}>
                                        مشخصات وارد شده اشتباه است
                                    </Text>
                                    <Icon name="ios-alert" style={styles.errorIcon} />
                                </View>
                            )}
                            <Icon name="ios-log-in" style={{color:colors.base,fontSize:70,marginBottom:20}}/>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(mobile) => this.setState({mobile})}
                                       value={this.state.mobile}
                                       keyboardType="numeric"
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="شماره تلفن همراه"/>
                                <Icon name="ios-call" style={styles.inputIcon}/>
                            </Item>
                            <Item rounded style={styles.input}>
                                <Input onChangeText={(password) => this.setState({password})}
                                       value={this.state.password}
                                       secureTextEntry={true}
                                       style={{textAlign: 'right', paddingRight: 10}}
                                       placeholder="کلمه عبور"/>
                                <Icon name="ios-lock" style={styles.inputIcon}/>
                            </Item>

                            <Button block style={[styles.button,{backgroundColor:colors.base}]} onPress={this.onSubmit}>
                                <Text style={[styles.buttonText,{color:colors.white}]}>ورود</Text>
                            </Button>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                <View>
                                    <Text style={{color:colors.base, textAlign:'center',paddingTop:20}}>
                                        رمز عبور خود را فراموش کرده اید؟
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                );
                break;
            case 1:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <View style={{alignItems:'center'}}>
                                <Spinner />
                            </View>
                        </Content>
                    </Container>
                );
                break;
            case 2:
                return (
                    <Container style={styles.container}>
                        <Content>
                            <Text style={{textAlign: 'center', padding:20}}>
                                شما با موفقیت وارد شدید
                            </Text>
                            <Button block large style={{margin: 20}} onPress={this.close}>
                                ادامه
                            </Button>

                        </Content>
                    </Container>
                );
                break;
            default:
                return(
                    <View>

                    </View>
                )
        }

    }
}


function mapStateToProps(state, ownProps) {
    return {
        users: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)