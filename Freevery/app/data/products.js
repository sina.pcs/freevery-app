export default products = [
    {
        "id": 1,
        "title": "شیر کم چرب پگاه",
        "description": "توضیحات شیر کم چرب پگاه",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":2000,
        "parent":7,

    },
    {
        "id": 2,
        "title": "شیر پر چرب پگاه",
        "description": "توضیحات شیر پر چرب پگاه",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":2000,
        "parent":7,
    },
    {
        "id": 3,
        "title": "شیر کم چرب دامداران",
        "description": "توضیحات شیر کم چرب دامداران",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":2000,
        "parent":7,
    },
    {
        "id": 4,
        "title": "پفک چی توز",
        "description": "توضیحات پفک چی توز",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":1000,
        "parent":7,
    },
    {
        "id": 5,
        "title": "ماست کم چرب پگاه",
        "description": "توضیحات ماست کم چرب پگاه",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":1500,
        "parent":7,
    },
    {
        "id": 6,
        "title": "ماست پر چرب پگاه",
        "description": "توضیحات ماست پر چرب پگاه",
        "image": "http://footbin1.in/6260176820015.jpg",
        "price":1500,
        "parent":8,
    }

];