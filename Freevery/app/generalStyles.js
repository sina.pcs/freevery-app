import {StyleSheet} from 'react-native';

export const generalStyles = StyleSheet.create({
    darkColor:{
        color:'#333'
    },
    textRight:{
        textAlign:'right'
    },
    flex1:{
        flex:1
    },
    iranSans:{
      fontFamily:'IRANSansMobile'
    }
});