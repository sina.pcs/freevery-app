import React, { Component } from 'react';
import Application from './app/Application';
import {
    AppRegistry, AsyncStorage
} from 'react-native';

import {Provider} from 'react-redux';

import configureStore from './app/redux/store/configureStore';

const store = configureStore();
import PushNotification from 'react-native-push-notification';
import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import {STORAGE_KEY} from "./app/constants/AppConstants";


export default class Freevery extends Component {
    constructor(){
        super();
        this.state = {
            orderId:null
        }
    }
    componentDidMount(){
        let _this = this;
        this.getUser().done(user => {
            PushNotification.configure({
                // (optional) Called when Token is generated (iOS and Android)
                onRegister: function(token) {
                    _this.getDevice().done((device) => {
                        if(!device || device.game_version != DeviceInfo.getVersion() || device.token != token.token || !device.id || (user && device.user_id != user.id)){
                            let objectToSend = {
                                identifier: token.token,
                                device_type: "1",
                                game_version: DeviceInfo.getVersion(),
                                device_model: DeviceInfo.getModel(),
                                device_os: DeviceInfo.getSystemName() + DeviceInfo.getSystemVersion(),
                                sdk:DeviceInfo.getUserAgent(),
                                user_id: user?user.id:''
                            };

                            //update token
                            if(device && (device.token !== token.token || device.game_version != DeviceInfo.getVersion()) ){
                                objectToSend.previous_token = device.token;
                            }

                            axios.post('https://freevery.com/api/web/push/register-token',
                                objectToSend
                            )
                                .then(function (result) {
                                    if(result.data.code == '1'){
                                        let id = JSON.parse(result.data.data).id;
                                        let device = {
                                            token: token.token,
                                            id:id,
                                            game_version: DeviceInfo.getVersion(),
                                            user_id: user.id
                                        };
                                        _this.storeDevice(JSON.stringify(device)).done(()=>{

                                        });
                                    }
                                })
                                .catch(function (error) {
                                    console.log(error);
                                })

                        }
                    });

                },

                // (required) Called when a remote or local notification is opened or received
                onNotification: function(notification) {
                    const clicked = notification.userInteraction;
                    const foreground = notification.foreground;

                    let notificationData;
                    if(notification.custom)
                        notificationData = JSON.parse(notification.custom);

                    if ((notificationData && notificationData.a && notificationData.a.order_id) && (clicked)) {
                        _this.setState({
                            orderId:notificationData.a.order_id
                        })
                    } else {
                        PushNotification.localNotification({
                            title: notification.title, // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
                            message: notification.alert,
                            custom: notification.custom// (required)
                        });
                    }
                },

                // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
                senderID: "321527302730",

                // IOS ONLY (optional): default: all - Permissions to register.
                permissions: {
                    alert: true,
                    badge: true,
                    sound: true
                },


                // Should the initial notification be popped automatically
                // default: true
                popInitialNotification: true,

                /**
                 * (optional) default: true
                 * - Specified if permissions (ios) and token (android and ios) will requested or not,
                 * - if not, you must call PushNotificationsHandler.requestPermissions() later
                 */
                requestPermissions: true,
            });
        });
    }

    getUser = async () => {
        try {
            let user = await AsyncStorage.getItem('@freevery:user');
            if (user !== null){
                user = JSON.parse(user);
            }
            return user;
        } catch (error) {
            return null;
        }
    };

    getDevice = async () => {
        try {
            let device = await AsyncStorage.getItem('@freevery:device');
            if (device !== null){
                device = JSON.parse(device);
            }
            return device;
        } catch (error) {
            return false;
        }
    };
    storeDevice = async (device) => {
        try {
            await AsyncStorage.setItem('@freevery:device',device);
        } catch (error) {

        }
    };
  render() {
        return (
            <Provider store={store}>
                <Application notificationClickedOrderId={this.state.orderId} />
            </Provider>
        );
    }
}


AppRegistry.registerComponent('Freevery', () => Freevery);
